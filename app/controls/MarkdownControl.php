<?php

namespace App\Controls;
use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Html;

/**
 * Class MarkdownControl
 * @package App\Control
 */
class MarkdownControl extends BaseControl
{
    /**
     * @param  string|object
     */
    public function __construct($label = null){
        parent::__construct($label);
        $this->control->setName('textarea');
        $this->setOption('type', 'markdown');
    }

    /**
     * Generates control's HTML element.
     * @return Html
     */
    public function getControl()
    {
        $this->setOption('rendered', true);
        $el = clone $this->control;

        $container = Html::el( 'div' );
        $container->addHtml( $el->addAttributes([
            'name' => $this->getHtmlName(),
            'id' => $this->getHtmlId(),
            'required' => $this->isRequired(),
            'disabled' => $this->isDisabled(),
            'style' => "width: 100%;",
        ] ));

        $divEl = Html::el( 'div' );
        $divEl->setAttribute('id', $this->getHtmlId()."-rendered");
        $container->addHtml( $divEl );

        $container->addHtml($this->getScript($this->getHtmlId()) );

        return $container;
    }

    /**
     * @param $id
     * @return string
     */
    private function getScript($id) {
        $script = "<script> $(function() { initMarkdown($(\"#".$id."\"), $(\"#".$id."-rendered\")); }); </script>";
        return $script;
    }

}