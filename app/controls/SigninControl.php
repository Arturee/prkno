<?php
namespace App\Controls;

use App\Exception\BugError;
use App\Model\Authenticator;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security;
use App\Model\UserRepository;
use Nette\Security\User;

/**
 * @author Artur
 * PERHAPS ajaxify it
 *
 * Class SigninControl
 * @package App\Control
 */
class SigninControl extends Control {
    /** @var callable[] */
    public $onLogin = [], $onLogout = [];

    /** @var Security\User */
    private $user;
    /** @var Translator */
    private $translator;
    /** @var string */
    private $expiration;
    /** @var UserRepository*/
    private $userRepository;

    public function __construct(
        User $user,
        Translator $translator,
        UserRepository $userRepository,
        string $expiration
    ) {
        parent::__construct();
        $this->user = $user;
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->expiration = $expiration;
    }

    /**
     * @Override
     */
    public function render() : void
    {
        $this->template->user = $this->user;
        $this->template->render(__DIR__ . '/SigninControl.latte');
    }

    public function handleOut() : void
    {
        if (!$this->user->isLoggedIn()){
            throw new BugError($this->translator->translate('_control.signIn.message.userNotLoggedIn'));
        }


        $this->user->logout();
        $this->onLogout($this);
    }

    protected function createComponentSigninForm() : Form
    {
        $form = new Form;
        $form->setTranslator($this->translator);
        $form->addText('email', '')
            ->addRule(Form::EMAIL, 'control.signin.message.badEmail') //only to display a correctly translated message
            ->setRequired('control.signin.message.enterEmail');
        $form->addPassword('password', '')
            ->setRequired('control.signin.message.enterPassword');
        $form->addSubmit('login', 'control.signin.form.signIn');
        $form->onSuccess[] = [$this, 'signIn'];
        return $form;

        //$form->addCheckbox('keep', 'page.sign.form.rememberme');
    }

    public function signIn(Form $form, $values) : void
    {
        if ($this->user->isLoggedIn()) {
            throw new BugError($this->translator->translate('control.signin.message.userNotLoggedIn'));
        }


        try {
            $email = $values->email;
            $password = $values->password;

            Authenticator::setDbAuthenticationMode();
            $this->user->login($email, $password);
            $this->user->setExpiration($this->expiration, true);
            //erases the user's identity form Nette after logout
            $this->onLogin($this);

        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }
}