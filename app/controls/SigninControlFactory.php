<?php
namespace App\Controls;

use Nette;
use Kdyby\Translation\Translator;
use Nette\Security\User;
use App\Model\UserRepository;

/**
 * @author Artur
 */
class SigninControlFactory {
    use Nette\SmartObject;

    /** @var User */
    private $user;
    /** @var  Translator */
    private $translator;
    /** @var string  */
    private $expiration;
    /** @var UserRepository*/
    private $userRepository;

    public function __construct(
        string $expiration,
        User $user,
        Translator $translator,
        UserRepository $userRepository
    ){
        $this->user = $user;
        $this->translator = $translator;
        $this->expiration = $expiration;
        $this->userRepository = $userRepository;
    }

    public function create() : SigninControl
    {
        return new SigninControl(
            $this->user,
            $this->translator,
            $this->userRepository,
            $this->expiration
        );
    }
}