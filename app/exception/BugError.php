<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 23-Sep-17
 * Time: 9:33 AM
 */

namespace App\Exception;
use Exception;

/**
 * @author Artur
 *
 * Class BugError
 *
 * Thrown when there is a bug in code - eg. in an impossible situation.
 * Used more or less like an assert statement.
 *
 * Log in detail and alert admin. Apologise to the user.
 *
 * @package App
 */
class BugError extends Exception {
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}