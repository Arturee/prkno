<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 23-Sep-17
 * Time: 9:33 AM
 */

namespace App\Exception;
use Exception;

/**
 * @author Artur
 *
 * Class NotImplementedError
 *
 * Signals that a function need to be implemented. This is good for making a quick API
 * and handling the implementation later.
 * This can only happen during development.
 *
 *
 * @package App
 */
class NotImplementedError extends BugError {
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}