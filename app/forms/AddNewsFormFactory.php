<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 03-Jan-18
 * Time: 9:03 AM
 */

namespace App\Forms;
use App\Model\NewsRepository;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;


class AddNewsFormFactory {
    use Nette\SmartObject;

    const
        SHORT_NAME_MAX_LENGTH = 45;

    /** @var NewsRepository */
    private $newsRepository;
    /** @var FormFactory */
    private $formFactory;
    /** @var User */
    private $user;


    public function __construct(
        FormFactory $formFactory,
        NewsRepository $newsRepository,
        User $user
    ) {
        $this->formFactory = $formFactory;
        $this->newsRepository = $newsRepository;
        $this->user = $user;
    }


    public function create() : Form
    {
        $form = $this->formFactory->create();
        $form->addText('titleCS', 'control.news.add.titleCS')
            ->setRequired();
        $form->addTextarea('contentCS', 'control.news.add.contentCS')
            ->setRequired();
        $form->addText('titleEN', 'control.news.add.titleEN')
            ->setRequired();
        $form->addTextarea('contentEN', 'control.news.add.contentEN')
            ->setRequired();
        $form->addCheckbox('important', 'control.news.add.important');
        $form->addCheckbox('public', 'control.news.add.public');
        $form->addSubmit('submit', 'control.news.add.submit');
        $form->onSuccess[] = [$this, 'succeeded'];
        return $form;
    }

    public function succeeded(Form $form, $values) : void
    {
        $titleCS = $values["titleCS"];
        $contentCS = $values["contentCS"];
        $titleEN = $values["titleEN"];
        $contentEN = $values["contentEN"];
        $important = $values["important"];
        $public = $values["public"];
        $this->newsRepository->insertNews(
            $this->user->getId(),
            $titleCS,
            $contentCS,
            $titleEN,
            $contentEN,
            $important,
            $public
        );
    }

}