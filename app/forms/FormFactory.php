<?php

namespace App\Forms;

use Kdyby\Translation\Translator;
use Nette;
use Nette\Application\UI\Form;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Bs3FormRenderer adds automatic bootstrap styling. However, it would be nice to be able to do
 * custom lattes for our controls/forms, but that is currently impossible - Markdown control
 * stops working then.
 *
 *
 * How to render custom lattes:
 *  see SigninControl.latte
 *  to render selectize correctly it is necessary to set class=selectize!
 *
 *
 *  <div class="form-group">
<label n:name="students">Students:</label>
<input n:name="students" class="form-control selectize"/>
</div>
 *
 *
 *
 * Class FormFactory
 * @package App\Forms
 */
class FormFactory
{
    use Nette\SmartObject;

    /** @var  Translator */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }
    public function create() : Form
    {
        $form = new Form();
        $form->setRenderer(new Bs3FormRenderer());
        $form->setTranslator($this->translator);
        return $form;
    }
}
