<?php

namespace App\Forms;

use App\Controls\MarkdownControl;
use App\Model\DocumentRepository;
use App\Model\ProjectRepository;
use App\Model\ProposalRepository;
use App\Model\UserOnProject;
use App\Model\UserOnProjectRepository;
use App\Model\UserRepository;
use Nette;
use Nette\Application\UI\Form;
use Kdyby\Translation\Translator;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class NewProjectFormFactory
 * @package App\Forms
 */
class NewProjectFormFactory
{
    use Nette\SmartObject;

    const
        SHORT_NAME_MAX_LENGTH = 45;

    /** @var  ProjectRepository */
    private $projectRepository;
    /** @var ProposalRepository */
    private  $proposalRepository;
    /** @var DocumentRepository */
    private $documentRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var UserOnProjectRepository */
    private $userOnProjectRepository;
    /** @var Translator */
    public $translator;

    /**
     * NewProjectFormFactory constructor.
     * @param ProjectRepository $projectRepository
     * @param ProposalRepository $proposalRepository
     * @param DocumentRepository $documentRepository
     * @param UserRepository $userRepository
     * @param UserOnProjectRepository $userOnProjectRepository
     * @param Translator $translator
     */
    public function __construct(
        ProjectRepository $projectRepository,
        ProposalRepository $proposalRepository,
        DocumentRepository $documentRepository,
        UserRepository $userRepository,
        UserOnProjectRepository $userOnProjectRepository,
        Translator $translator
    ) {
        $this->projectRepository = $projectRepository;
        $this->proposalRepository = $proposalRepository;
        $this->documentRepository = $documentRepository;
        $this->userRepository = $userRepository;
        $this->userOnProjectRepository = $userOnProjectRepository;
        $this->translator = $translator;
    }


    /**
     * @return Form
     */
    public function create() {
        $form = new Form();

        $form->addText('name', $this->translator->translate('control.newProject.form.name') . ':')
            ->setRequired(true)
            ->getControlPrototype()->setAttribute('class', 'form-control');

        $form->addText('abbr', $this->translator->translate('control.newProject.form.abbr') . ':')
            ->addRule(Form::MAX_LENGTH, NULL, self::SHORT_NAME_MAX_LENGTH)
            ->setRequired(true)
            ->getControlPrototype()->setAttribute('class', 'form-control');

        $form->addText('supervisor', $this->translator->translate('control.newProject.form.supervisor') . ':')
            ->setDisabled('true')
            ->getControlPrototype()->setAttribute('class', 'form-control');

        $form->addHidden('supervisor_id');

        $form->addSelectize('consultants', $this->translator->translate('control.newProject.form.consultants') . ':',
            $this->userRepository->getAll());

        $form->addSelectize('students', $this->translator->translate('control.newProject.form.students') . ':',
            $this->userRepository->getAll());

        $form->addText('description', $this->translator->translate('control.newProject.form.description') . ':')
            ->setRequired(false)
            ->getControlPrototype()->setAttribute('class', 'form-control');

        $form['proposal'] = new MarkdownControl($this->translator->translate('control.newProject.form.proposal') . ':');

        $form->addUpload('proposal_file', $this->translator->translate('control.newProject.form.uploadProposal') . ':')
            ->setRequired(false)
            ->getControlPrototype()->setAttribute('class', 'form-control');

        // TODO save draft
        //$form->addSubmit('send', $this->translator->translate('page.projects.new.form.send'));
        $form->addSubmit('send', $this->translator->translate('control.newProject.form.send'))
            ->getControlPrototype()->setAttribute('class', 'btn btn-default');

        $form->onSuccess[] = [$this, 'newProjectFormSucceeded'];

        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = "div class='form-group'";
        $renderer->wrappers['label']['container'] = null;
        $renderer->wrappers['control']['container'] = null;

        return $form;
    }

    /**
     *
     *
     * @param $form
     * @param $values
     * @return
     */
    public function newProjectFormSucceeded(Form $form, $values) {
        try {
            $projectId = $this->projectRepository->insertProject($values['name'], $values['abbr'], $values['description']);
            $newProject = $this->projectRepository->getById($projectId);

            //TODO check whether the user exists

            $this->userOnProjectRepository->insertUserOnProject(intval($values['supervisor_id']), $projectId,
                UserOnProject::PROJECT_ROLE_SUPERVISOR);

            $students = $values['students'];

            if ($students != null) {
                foreach ($values['students'] as $student) {
                    $this->userOnProjectRepository->insertUserOnProject($student, $projectId,
                        UserOnProject::PROJECT_ROLE_TEAM_MEMBER);
                }
            }
            $consultants = $values['consultants'];
            if ($consultants != null) {
                foreach ($consultants as $consultant) {
                    $this->userOnProjectRepository->insertUserOnProject($consultant, $projectId,
                        UserOnProject::PROJECT_ROLE_CONSULTANT);
                }
            }
            //$newProject->setUsersOnProject(array($this->userOnProjectRepository->getUsersOnProject($projectId)));

            // TODO $value['proposal'] insert as Document, get documentId, insert proposal with proper id
            //$this->documentRepository->insertDocument($values['proposal'], null, null);
            //$this->proposalRepository->insertProposal($projectId, null);

        } catch (Exception $e) { //TODO specify
            $form->addError($this->translator->translate('control.newProject.message.abbrTaken'));
            return;
        }
    }

}