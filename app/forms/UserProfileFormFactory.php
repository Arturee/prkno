<?php
namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model\UserRepository;
use Nette\Security\User;
use Kdyby\Translation\Translator;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 *
 * ARTUR TODO rewrite using User class instead of $row.
 *
 * Class UserProfileFormFactory
 * @package App\Forms
 */
class UserProfileFormFactory
{
	use Nette\SmartObject;

	const
		PASSWORD_MIN_LENGTH = 8,
		USERNAME_MIN_LENGTH = 4,
		USERNAME_MAX_LENGTH = 30;

	/** @var User */
	private $user;
	/** @var  UserRepository */
	private $userRepository;
	/** @var Translator */
	public $translator;

	public function __construct(User $user, UserRepository $userRepository, Translator $translator)
	{
		$this->user = $user;
		$this->userRepository = $userRepository;
		$this->translator = $translator;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = new Form();

		//ARTUR TODO using an @internal method ->setValue() is NOT ok
		$form->addText('ukco', $this->translator->translate('control.userProfile.form.ukco') . ':')
			->setDisabled()->setValue('readonly value');
		$form->addText('role', $this->translator->translate('control.userProfile.form.role') . ':')
			->setDisabled()->setValue('readonly value');
		$form->addText('username', $this->translator->translate('control.userProfile.form.username') . ':')
			->addRule(Form::MIN_LENGTH, NULL, self::USERNAME_MIN_LENGTH)
			->addRule(Form::MAX_LENGTH, NULL, self::USERNAME_MAX_LENGTH)
			->setRequired(false);

		$form->addEmail('email', $this->translator->translate('control.userProfile.form.email') . ':')
			->setRequired($this->translator->translate('control.userProfile.message.enterEmail'));

		$form->addPassword('password', $this->translator->translate('control.userProfile.form.password') . ':')
			->setOption('description', $this->translator->translate(
                    'control.userProfile.message.minPassword',
                ['%minlength%' => self::PASSWORD_MIN_LENGTH]))
			->addRule(Form::MIN_LENGTH, NULL, self::PASSWORD_MIN_LENGTH)
			->setRequired(false);

		$form->addPassword('passwordVerify', $this->translator->translate('control.userProfile.form.passwordVerify') . ':')
     		->addConditionOn($form['password'], Form::FILLED, TRUE)
    			->setRequired($this->translator->translate('control.userProfile.message.passwordNotEqual'))
    			->addRule(Form::EQUAL, $this->translator->translate('control.userProfile.message.passwordNotEqual'),
                    $form['password']);

		$form->addText('custom_url', $this->translator->translate('control.userProfile.form.customUrl') . ':')
			->addRule(Form::URL, NULL, NULL)
			->setRequired(false);

		$form->addSubmit('send', $this->translator->translate('control.userProfile.form.send'));

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				unset($values['passwordVerify']);

				$this->userManager->updateValues($this->user->id, $values);

			} catch (Exception $e) { //TODO specify
				$form['username']->addError($this->translator->translate('control.userProfile.message.userNameTaken'));
				return;
			}
			$onSuccess();
		};

		return $form;
	}

	public function createEdit(callable $onSuccess) {
    	$form = $this->create($onSuccess);
    	$id = $this->user->id;
    	$row = $this->userManager->getUserById($id); //TODO this returns $user tho!
    	$form->setDefaults($row); //TODO this works?
        return $form;
	}

}
