<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 24-Dec-17
 * Time: 8:52 AM
 */
namespace App\Lang;

class Lang {
    const
        CS = 'cs',
        EN = 'en';
}