Rules of language translation:

Use camel case.


# page.cs_CZ.neon: (presenters) #
layout:
    (stuff from layout.latte)
    ...
[presenter name]:
    (stuff from presenter's latte)
    title:
    ...

# control.cs_CZ.neon: (controls, form factories) #
[control name]
    (stuff from control's latte)
    message:
        (flash messages displayed to the user - usually error messages (bad password etc.))
    ...

# exc.cs_CZ.neon: #
(messages for exceptions - Only if we will later display that message to the user)





Doc:
we do not use a "common.cs_CZ.neon" file for frequently used words,
because it leads to bad translations, and it's hard to tell what
should be common and what not.

(eg. project/user name -> název projektu vs. jméno uživatele)

Also it is prone to errors with inflection (user -> uživatel/uživatelům/uživatele).