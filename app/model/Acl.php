<?php
namespace App\Model;

use Nette;
use Nette\Security\Permission;

class Acl {
	use Nette\SmartObject;

	const
        RESOURCE_DASHBOARD = 'dashboard',
        RESOURCE_PROJECTS = 'projects', //should be split to search and create probably
        RESOURCE_ADS = 'ads',
        RESOURCE_USERS = 'users',
        RESOURCE_ERROR_CODES = 'error-codes',
        RESOURCE_STATIC_PAGES = 'static-pages',
        RESOURCE_NEWS = 'news',
        RESOURCE_USER_PROFILE = 'user-profile';

	const
        ACTION_CREATE = 'create',
        ACTION_VIEW = 'view',
        ACTION_VIEW_PRIVATE = 'view-private';




	/** @return Permission */
	public static function create() : Permission
    {

		$acl = new Permission;

        /**
         * Roles inherit each form the previous.
         *
         * Privileges can be denied:
         * eg. $acl->deny('guest', 'backend');
         */
		$acl->addRole(User::ROLE_GUEST);
		$acl->addRole(User::ROLE_STUDENT, [User::ROLE_GUEST]);
        $acl->addRole(User::ROLE_ACADEMIC, [User::ROLE_STUDENT]);
        $acl->addRole(User::ROLE_COMMITTEE_MEMBER, [User::ROLE_ACADEMIC]);
        $acl->addRole(User::ROLE_COMMITTEE_CHAIR, [User::ROLE_COMMITTEE_MEMBER]);
        $acl->addRole(User::ROLE_COMMITTEE_SECRETARY, [User::ROLE_COMMITTEE_CHAIR]);


        /**
         * Dashboard
         */
        $acl->addResource(Acl::RESOURCE_DASHBOARD);
        $acl->allow(User::ROLE_GUEST, Acl::RESOURCE_DASHBOARD, Acl::ACTION_VIEW);
        /**
         * Projects
         */
        $acl->addResource(Acl::RESOURCE_PROJECTS);
        $acl->allow(User::ROLE_ACADEMIC, Acl::RESOURCE_PROJECTS, Acl::ACTION_VIEW);
        /**
         * Ads
         */
        $acl->addResource(Acl::RESOURCE_ADS);
        $acl->allow(User::ROLE_GUEST, Acl::RESOURCE_ADS, Acl::ACTION_VIEW);
        /**
         * Users
         */
        $acl->addResource(Acl::RESOURCE_USERS);
        $acl->allow(User::ROLE_COMMITTEE_MEMBER, Acl::RESOURCE_USERS, Acl::ACTION_VIEW);
        /**
         * User profile
         */
        $acl->addResource(Acl::RESOURCE_USER_PROFILE);
        $acl->allow(User::ROLE_STUDENT, Acl::RESOURCE_USER_PROFILE, Acl::ACTION_VIEW);
        /**
         * Static pages
         */
        $acl->addResource(Acl::RESOURCE_STATIC_PAGES);
        $acl->allow(User::ROLE_GUEST, Acl::RESOURCE_STATIC_PAGES, Acl::ACTION_VIEW);
        /**
         * Error codes
         */
        $acl->addResource(Acl::RESOURCE_ERROR_CODES);
        $acl->allow(User::ROLE_COMMITTEE_MEMBER, Acl::RESOURCE_ERROR_CODES, Acl::ACTION_VIEW);
        /**
         * News
         */
        $acl->addResource(Acl::RESOURCE_NEWS);
        $acl->allow(User::ROLE_COMMITTEE_CHAIR, Acl::RESOURCE_NEWS, Acl::ACTION_CREATE);
        $acl->allow(User::ROLE_STUDENT, Acl::RESOURCE_NEWS, Acl::ACTION_VIEW_PRIVATE);
		return $acl;
	}
}