<?php
namespace App\Model;

use Nette;
use Nette\Security\Identity;
use Nette\Security\AuthenticationException;
use Nette\Utils\DateTime;
use Kdyby\Translation\Translator;
use Nette\Security\IAuthenticator;
use Nette\Security\Passwords;
use Nette\Database;

/**
 * @author Artur
 *
 * LATER what if user stops being authenticated with CAS but stays with PRKNO?
 *
 * It is impossible to have multiple authenticators as services in Nette (exceptions is thrown).
 * Therefore, we have a single authenticator that can switch modes via a static method.
 *
 * Class Authenticator
 * @package App\Model
 */
class Authenticator implements IAuthenticator {
	use Nette\SmartObject;

	/** @var UserRepository */
	private $userRepository;
	/** @var Translator */
	private $translator;
    /** @var CasClient */
    private $casClient;
    /** @var bool */
    private static $casMode = true;

    public function __construct(UserRepository $userRepository, Translator $translator, CasClient $casClient)
    {
        $this->userRepository = $userRepository;
        $this->translator = $translator;
        $this->casClient = $casClient;
    }


    /**
     * Authenticates via CAS.
     * If self::setDbAuthenticationMode() is called, then authenticates via DB,
     * in which case credentials must be supplied.
     *
     * @param array $credentials
     * @return Identity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials) : Identity
    {
        if (self::$casMode){
            $identity = $this->authenticateViaCas($credentials);
        } else {
            $identity = $this->authenticateViaDb($credentials);
        }
        return $identity;
    }

    /**
     * Switches auth mode to via DB. Cannot be undone.
     */
    public static function setDbAuthenticationMode() : void
    {
        self::$casMode = false;
    }


























    /**
     * @Override
     * @param array $credentials empty (they get downloaded from CAS automatically)
     * @return Identity
     * @throws AuthenticationException
     */
    private function authenticateViaCas(array $credentials) : Identity
    {
        $email = $this->casClient->getCurrentUserEmail();
        $user = $this->userRepository->getUserByEmail($email);

        if (!$user){
            //this is the user's first login ever (via CAS)
            $user = $this->registerUserViaCas();
        }

        if ($user->getLoginOption() === User::LOGIN_OPTION_DB){
            /**
             * 80% probability - a manual user gained CAS assess and logged in via CAS, which he SHOULD NOT DO
             * 10% probability - a manual user stole the email from a future cas user
             * 7% probability - a deleted user is blocking the email
             * 3% probability - a bug
             *
             */
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.cas.loginOptionDb')
            );
        }
        $this->assertNotBanned($user);


        $identity = $this->createIdentity($user);
        return $identity;
    }

    private function registerUserViaCas() : User
    {
        try {
            $casInfo = $this->casClient->downloadCurrentUserInfo();
            $id = $this->userRepository->insertUserFromCas($casInfo);
            /** @var User $user */
            $user = $this->userRepository->getById($id);
            return $user;

        } catch(Database\UniqueConstraintViolationException $e){
            //99% probability - a deleted user blocks this email in db
            //1% probability - a bug
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.cas.emailBlockedByADeletedUser')
            );
        }
    }

    /**
     *
     * @Override
     * @param array $credentials
     * @return Identity
     * @throws AuthenticationException
     */
    private function authenticateViaDb(array $credentials) : Identity
    {
        /**
         * @var string $email
         * @var string $password
         */
        $email = $credentials[IAuthenticator::USERNAME];
        $password = $credentials[IAuthenticator::PASSWORD];

        $user = $this->userRepository->getUserByEmail($email);

        if (!$user) {
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.db.badEmail')
            );
        }
        if ($user->getLoginOption() === User::LOGIN_OPTION_CAS){
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.db.mustLoginViaCas')
            );
        }

        $this->assertNotBanned($user);

        if (!Passwords::verify($password, $user->getPasswordHash())) {
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.db.badPassword')
            );
        }


        $identity = $this->createIdentity($user);
        return $identity;
    }

    /**
     * @throws AuthenticationException
     */
    private function assertNotBanned(User $user) : void
    {
        if ($user->isBanned()) {
            throw new AuthenticationException(
                $this->translator->translate('exc.auth.userBanned')
            );
        }
    }

	private function createIdentity(User $user) : Identity
    {
		$now = new DateTime();
		$this->userRepository->updateValues($user,
			[ UserRepository::COLUMN_LAST_LOGIN => $now ]
		);
		$user->destroyPasswordHash();
		//makes sure password hash is not stored in session
        $identity = new Identity($user->getId(), $user->getRole(), ['user'=>$user]);
		return $identity;


		/**
         * CARE nette identity/user data cannot be accessed anyway, so we will have to get them form the
         * db every time we need them. This is just for debugging purposes.
         */
	}
}