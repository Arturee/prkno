<?php
namespace App\Model;

use Nette;
use phpCAS;
use App\Exception\BugError;

/**
 *
 * phpCAS doc examples:
 * https://wiki.jasig.org/display/CASC/phpCAS+examples
 *
 * Currently using phpCAS version 1.3.5
 * API doc:
 * http://developer.jasig.org/cas-clients/php/1.3.5/docs/api/classphpCAS.html
 *
 *
 *
 * cuni.cz CAS config:
 * https://supercomp.cuni.cz/wiki/index.php/Pravidla_pou%C5%BEit%C3%AD_CAS#Vztahy_k_UK
 *
 * There seems to be no error log for phpCAS! (only a bloated debug log)
 *
 * TODO find out if exceptions can arise and handle them.
 * TODO expect and log all invalid values got from CAS! (hard checks)
 *
 *
 */
class CasClient {
	use Nette\SmartObject;

	const
        /**
         * (eg. Petr Novak)
         */
        ATTRIBUTE_FULL_NAME = 'cn',
        /**
         * can be missing (eg. novakp@mff.cuni.cz or novakp@gmail.com)
         */
        ATTRIBUTE_EMAIL = 'mail',






        /**
         * can be multiple or missing (eg. staff@mff.cuni.cz)
         *
         * CARE not accessible from localhost
         * TODO try from cuni.cz, we need this one!
         *
         * also relevant might be:
         *  -   eduPersonAffiliation - can be multiple or missing (eg. staff)
         *  -   eduPersonPrimaryAffiliation - can be missing  (eg. staff)
         *  -   eduPersonOrgDN (eg. mff dc=mff,dc=cuni,dc=cz)
         *
         */
        ATTRIBUTE_AFFILIATIONS = 'eduPersonScopedAffiliation',
        /**
         * CARE not accessible from localhost
         * TODO try from cuni.cz
         *
         * If the code runs on the domain cuni.cz, then UKCO is returned. (eg. 12341234)
         * Otherwise, a randomized token is returned. (eg. JA4Gsujf7Pw+6zdptGuINpYxRoo=)
         */
        ATTRIBUTE_UKCO = 'cuniPersonalId',
        /**
         * CARE not accessible from localhost
         * TODO try from cuni.cz
         * can be multiple or missing
         */
        ATTRIBUTE_USERNAME = 'uid',
        /**
         * CARE not accessible from localhost
         * TODO try from cuni.cz
         * can be missing
         */
        ATTRIBUTE_PREFERRED_LANGUAGE = 'preferredLanguage';




	public function __construct(string $hostname, int $port, string $relativeUrl, string $appDir)
	{
        /**
         * set the location of debug log
         * MAYBE factor this path out to logger for better clarity
         * LATER: this generates a log of lines in log, maybe create a mechanism to delete the log periodically
         */
        phpCAS::setDebug($appDir . '/../log/cas.log');
        /**
         * configure CAS client
         */
        $CAS_VERSION = '3.0'; //PERHAPS how do we know this?
        phpCAS::client($CAS_VERSION, $hostname, $port, $relativeUrl);
        /**
         * Disable SSL validation for the CAS server
         *
         * LATER we could trun the SSL validation back ON
         * CARE this function cannot be called before phpCAS::client
         */
        phpCAS::setNoCasServerValidation();
	}


    /**
     * Redirects to CAS web page, where the user logs in and after a successful login, he is
     * redirected back to the original url.
     */
	public function redirectToCasForAuthentication() : void
    {
        phpCAS::forceAuthentication();
    }
    public function isCurrentUserAuthenticated() : bool
    {
        return phpCAS::isAuthenticated();
    }
    /**
     * Redirects to CAS web page, which logs the user out and immediately redirects back to
     * our url argument.
     */
    public function logout(string $urlToRedirectBackTo) : void
    {
        phpCAS::logoutWithRedirectService($urlToRedirectBackTo);
    }
    public function getCurrentUserEmail() : string
    {
        if (!$this->isCurrentUserAuthenticated()){
            throw new BugError('not authenticated via CAS');
        }

        $email = phpCAS::getAttribute(self::ATTRIBUTE_EMAIL);
        return $email;
    }
    public function downloadCurrentUserInfo() : CasInfo
    {
        if (!$this->isCurrentUserAuthenticated()){
            throw new BugError('not authenticated via CAS');
        }

        $fullName = phpCAS::getAttribute(self::ATTRIBUTE_FULL_NAME);
        $email = phpCAS::getAttribute(self::ATTRIBUTE_EMAIL);
        $affiliations = phpCAS::getAttribute(self::ATTRIBUTE_AFFILIATIONS);
        if (!$affiliations){
            $affiliations = [];
        }
        $role = self::extractRole($affiliations);
        $info = new CasInfo($fullName, $email, $role);
        return $info;
    }






    /**
     * TODO take care of the details during testing
     *
     * @param string[] $affiliations eg. [ staff@mff.cuni.cz, student@mff.cuni.cz ]
     * @return string
     */
	private static function extractRole(array $affiliations) : string
    {
        $role = User::ROLE_GUEST;

        $CUNI_CZ = preg_quote('.cuni.cz') . '$/';;

        $patternsStudent = [];
        $patternsStudent[] = '/^student@.*' . $CUNI_CZ;
        $patternsStudent[] = '/^interrupted-student@.*' . $CUNI_CZ;
        $patternsStudent[] = '/^applicant@.*' . $CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent)){
            $role = User::ROLE_STUDENT;
        }

        $MFF_CUNI_CZ = preg_quote('@mff.cuni.cz') . '$/';;

        $patternsStaff = [];
        $patternsStaff[] = '/^staff' . $MFF_CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent)){
            $role = User::ROLE_ACADEMIC;
        }

        $patternsStudent2 = [];
        $patternsStudent2[] = '/^member@.*' . $CUNI_CZ;
        $patternsStudent2[] = '/^affiliate@.*' . $CUNI_CZ;
        if (self::hasMatch($affiliations, $patternsStudent2)){
            $role = User::ROLE_STUDENT;
        }

        return $role;



        //EMAIL Krulis:
        //        1) pokud je tam student, interrupted-student, nebo applicant @*.cuni.cz -> je to student
        //2) pokud je tam staff@mff.cuni.cz -> je to ucitel
        //3) pokud je tam member nebo affiliate @*.cuni.cz -> je to student (asi)
        //4) pokud je cokoli jineho, mel by se mu vytvorit ucet, ale mel by videt stejne jako anonymni uzivatel
        //(tajemnik/vedouci ho muzou rucne zmenit na studenta nebo ucitele, takze je potreba zalozit ten ucet).
        //
        //Vsimnete si, ze ucitel je az v bode 2, kdyz NENI jasne definovany student. Pokud k tomu mate pripominky nebo
        //dotazy, ptejte se.


        /**
         * AFFILIATIONS in API:
         *
            staff 	Zaměstnanci - osoby s pracovní smlouvou, dohodou o provedení práce nebo dohodou o provedení činnosti.
            student 	Studenti všech forem studia včetně studentů programu Erasmus.
            interrupted-student 	Studenti s přerušeným studiem.
            applicant 	Uchazeči přijatí ke studiu, ale ještě nikoliv studenti, tj. před zápisem.
            member 	Účastníci kurzů CŽV, externí uživatelé služeb nárokovaní fakultami, studenti CERGE.
            affiliate 	Stážisté.
            guest 	Držitelé nepersonalizovaných průkazů.
            alumn 	Absolventi - studenti, kteří úspěšně dokončili studium.
         */
    }
    /**
     * @param string[] $affiliations
     * @param string[] $patterns
     * @return bool
     */
    private static function hasMatch(array $affiliations, array $patterns) : bool
    {
        foreach($affiliations as $affiliation){
            foreach ($patterns as $pattern){
                if (preg_match($pattern, $affiliation)){
                    return true;
                }
            }
        }
        return false;
    }
}
