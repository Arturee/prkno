<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 21-Sep-17
 * Time: 10:08 PM
 */

namespace App\Model;
use Nette;


/**
 * @author Artur
 *
 * Class CasInfo
 * @package App\Model
 */
class CasInfo
{
    use Nette\SmartObject;

    /** @var  string  */
    private $fullName, $email, $role;


    public function __construct(string $fullName, string $email, string $role)
    {
        $this->fullName = $fullName;
        $this->email = $email;
        $this->role = $role;
    }



    public function getFullName(): string
    {
        return $this->fullName;
    }
    public function getEmail(): string
    {
        return $this->email;
    }
    public function getRole(): string
    {
        return $this->role;
    }

}