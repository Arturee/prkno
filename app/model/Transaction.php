<?php
namespace App\Model;

use Nette;
use Nette\Database;

/**
 * Transaction processing
 */
class Transaction {
	use Nette\SmartObject;
	
	/** @var Database\Context */
	private $database;

	/**
	 * Create new class managin transaction processing
	 *
	 * @param $database 	Database dependency injection
	 */
	public function __construct(Database\Context $database) {
		$this->database = $database;
	}

	/**
	 * Begin transaction
	 */
	public function beginTransaction() {
		$this->database->beginTransaction();
	}

	/**
	 * Commit transaction
	 */
	public function commit() {
		$this->database->commit();
	}
}