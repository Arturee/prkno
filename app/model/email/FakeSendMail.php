<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Dec-17
 * Time: 8:25 AM
 */

namespace App\Email;
use Nette;
use Tracy\Debugger;


/**
 * To view what was sent open the file log/debug.log
 *
 * Class FakeSendMail
 * @package App\Utils
 */
class FakeSendMail implements Nette\Mail\IMailer {
    use Nette\SmartObject;



    public function send(Nette\Mail\Message $message): void
    {
        Debugger::log($message->generateMessage(), Debugger::DEBUG);
    }
}