<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 13-Dec-17
 * Time: 7:46 AM
 */

namespace App\Email;


use Nette;
use Nette\Application\LinkGenerator;
use Nette\Bridges\ApplicationLatte\ILatteFactory;

use App\Exception\NotImplementedError;
use App\Email\Mailer;

use App\Model\UserRepository;
use App\Model\UserOnProjectRepository;
use App\Model\ProjectRepository;

use App\Model\User;
use App\Model\Proposal;
use App\Model\DefenceDate;
use App\Model\Project;
use App\Model\News;
use App\Model\ProjectDefence;



/**
 *
 * Mail table allows us to have all emails and their implementations in one
 * place. This will come in handy when reviewing if all functionality is
 * implemented.
 *
 * Class MailTable
 * @package App\Utils
 *
 */
class MailScenarios
{
    use Nette\SmartObject;


    /** @var Mailer */
    private $mailer;
    /** @var LinkGenerator */
    private $linkGenerator;
    /** @var ILatteFactory */
    private $latteFactory;


    /** @var UserRepository */
    private $userRepository;
    /** @var UserOnProjectRepository */
    private $userOnProjectRepository;



    public function __construct(
        LinkGenerator $linkGenerator,
        ILatteFactory $latteFactory,
        Mailer $mailer,
        UserRepository $userRepository,
        UserOnProjectRepository $userOnProjectRepository
    )
    {
        $this->linkGenerator = $linkGenerator;
        $this->latteFactory = $latteFactory;
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
        $this->userOnProjectRepository = $userOnProjectRepository;
    }


    /** PROPOSALS ***************************************/
    /**
     * Send mail to all committee members about a proposal that
     * was just handed in.
     */
    public function proposalHandedIn(): void
    {
        $to = $this->userRepository->getAll();
        $subject = 'proposal handed in';
        $params = [
            'id'=>1,
            'name'=>'Projekt komise',
            'abbr'=>'PRKNO'
        ];
        $htmlMessage = $this->renderLatte('proposal-handed-in.latte', $params);
        $this->mailer->groupMail($to, $subject, $htmlMessage);

        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to all committee members that have not yet
     * voted about this proposal.
     *
     * every Monday at 8:00.
     */
    public function proposalDecisionReminder(Proposal $proposal): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to all committee members with the comment
     * that was made to this proposal and a link to it.
     */
    public function proposalCommented(User $committeMember, Proposal $proposal): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to all committee members and the supervisor
     * of this project, informing them that the proposal
     * was accepted and linking to the commented proposal.
     */
    public function proposalAccepted(Proposal $proposal): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to all committee members and the supervisor
     * of this project, informing them that the proposal
     * was declined and linking to the commented proposal.
     */
    public function proposalDeclined(Proposal $proposal): void
    {
        throw new NotImplementedError('Mail Table');
    }



    /** DEFENCES ***************************************/
    /**
     * Send mail to all committee members and news subscribers
     * that a new defence date is open. (Linking to the defense
     * date)
     */
    public function newDefenceDate(DefenceDate $defenceDate): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to all team members and the supervisor
     * of the project about the fact that their defence was scheduled
     * or re-scheduled for a concreted time of the day.
     */
    public function defenceScheduled(ProjectDefence $defence): void
    {
        throw new NotImplementedError('Mail Table');

    }
    /**
     * Send mail to all committee members who haven't voted yet about
     * their attendance at defenses on any suggested defense date.
     *
     * every Monday at 8:00
     * or right after defence voting was reactivated and some
     * committee members were newly marked as "not voted yet"
     */
    public function defenceAttendanceVoteReminder(DefenceDate $defenceDate): void
    {
        throw new NotImplementedError('Mail Table');

    }
    /**
     * Send mail to a committee member, who was selected
     * to attend at all/some defences on a particular date.
     */
    public function committeeMemberSelectedToAttend(DefenceDate $defenceDate, User $committee): void
    {
        throw new NotImplementedError('Mail Table');
    }

    /**
     * Send mail to all attending committee members, the
     * team on the project and their supervisor, reminding them
     * that a their defense will soon come.
     *
     * 1 week, 2 days and 2 hours before the defense
     */
    public function defenceIsComing(Project $project): void
    {
        throw new NotImplementedError('Mail Table');

    }
    /**
     * Send mail to all attending committee members, the
     * team on the project and their supervisor, informing them
     * that their defense was cancelled.
     */
    public function defenceCancelled(Project $project): void
    {
        throw new NotImplementedError('Mail Table');
    }






    /** DEADLINES ***************************************/
    /**
     * Send mail to the committee chair, informing him that
     * a project's deadline for implementation has passed, but
     * no defense date is open for the nest 30 days.
     * (Link that suggests a new date the upcoming Friday)
     */
    public function projectImplementationDeadlinePassed(Project $project): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to the committee chair, informing him that
     * a project's deadline for analysis has passed but no
     * defense date is open for the next 15 days.
     * (Link that suggests a new date the upcoming Friday)
     */
    public function projectAnalysisDeadlinePassed(Project $project): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to the committee chair, informing him that
     * a project, which has passed on condition has now
     * exceeded it's deadline for rework and should be checked.
     */
    public function conditionallyDefendedProjectDeadlinePassed(Project $project): void
    {
        throw new NotImplementedError('Mail Table');
    }






    /** MISC ***************************************/
    /**
     * Send mail to the team of a project and their supervisor,
     * informing them that their project has transferred into
     * a new stage/state.
     */
    public function projectStateChange(Project $project): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail about a new static page being published
     * to all committee members and news subscribers.
     */
    public function staticPagePublished(): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail with new news to all subscribers.
     */
    public function newsAdded(News $news): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to the author of an advertisement after
     * it was commented (unless he disabled these emails).
     */
    public function advertCommented(News $news): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to a user informing him that his role has changed.
     */
    public function userRoleWasChanged(User $user, string $oldRole): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to a user informing him that he is no longer
     * a supervisor/opponent/team member on some project. This usually
     * happens when the committee chair removes them from the project.
     */
    public function userLeftProject(User $user, Project $project, string $oldProjectRole): void
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to a user with a link to where he can change his
     * password. (applies only to those who aren't using CAS to log in)
     */
    public function passwordChangeLink(User $user, string $oldRole): void
    {
        throw new NotImplementedError('Mail Table');
    }





    /** TECHNICAL FAILURES ***************************************/
    /**
     * Send mail to the committee chair that there are problems
     * with CAS. (eg. the login page has crashed, etc.)
     */
    public function casConnectionFail()
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send mail to the committee chair, that something is wrong
     * with the database connection and the error has to
     * be resolved as soon as possible.
     */
    public function databaseConnectionFail()
    {
        throw new NotImplementedError('Mail Table');
    }
    /**
     * Send e-mail to committee chair, that error occurred
     * in system and has to be resolved as soon as possible.
     * (can be due to a bug)
     *
     */
    public function errorOccurred()
    {
        throw new NotImplementedError('Mail Table');
    }








    private function renderLatte(string $filename, array $params): string
    {
        $latte = $this->latteFactory->create();
        // install {link} and n:href macros
        Nette\Bridges\ApplicationLatte\UIMacros::install($latte->getCompiler());
        $latte->addProvider('uiControl', $this->linkGenerator);

        $html = $latte->renderToString(__DIR__ . '/emails/' . $filename, $params);
        return $html;
    }




    private function getTeamAndSupervisor(Project $project): array
    {
        throw new NotImplementedError('get news subscribers');
    }
    private function getChair(): User
    {
        throw new NotImplementedError('get news subscribers');
        //return $this->userRepository->getCommitteeChair();
    }
    /**
     * @return User[]
     */
    private function getCommittee(): array
    {
        throw new NotImplementedError('get news subscribers');
        //return $this->userRepository->getCommitteeMembers();
    }
    /**
     * @return User[]
     */
    private function getNewsSubscribers(): array
    {
        throw new NotImplementedError('get news subscribers');
    }
}