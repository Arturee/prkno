<?php
namespace App\Email;

use App\Exception\BugError;
use Nette;
use Nette\Mail\Message;
use Nette\Mail\IMailer;
use App\Model\User;






class Mailer {
	use Nette\SmartObject;

	/** @var Mailer */
	private $mailer;

	public function __construct(IMailer $mailer)
    {
        $this->mailer = $mailer;
    }


    /**
     * @param User[] $receivers
     * @param string $subject
     * @param string $content text including html links (<a href= ... >)
     */
	public function groupMail(array $receivers, string $subject, string $content): void
    {
        $from = 'robot@prkno.cz';
		$mail = new Message();
		$mail->setFrom($from);
		foreach ($receivers as $receiver) {
		    /** @var User $receiver */
			$address = $receiver->getEmail();
			if (!$address){
			    throw new BugError('email address is null');
            }
            $mail->addTo($address);
		}
    	$mail->setSubject($subject);
        $mail->setHtmlBody($content);
    	$this->mailer->send($mail);
	}

	public function mail(User $user, string $subject, string $content): void
    {
		$this->groupMail([$user], $subject, $content);
	}
}