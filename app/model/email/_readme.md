

To send emails always use MailScenarios.
MailScenarios generate emails via latte. We can use links
(eg. {link Projects:default} or n:href="Projects:default") and
variables (eg. {$projectName}) in those lattes, which is great.
(same as in classic presenter lattes)
It is also good to have the text of the emails separated from code. Emails are HTML, so any HTML can
be used in the lattes.

Lattes are located in ./emails/ and their names are hyphenated.

Emails are writte first part in czech and second part the same stuff in english.



# example usage: #
    code:
        $params = [
            'name'=>'Novy web projektove komise',
            'abbr'=>'PRKNO'
        ];
        $htmlMessage = $this->renderLatte('proposal-handed-in.latte', $params);


    latte:
        <span class=cze>
            Email ohledne {$name} (neboli {$abbr}) nalzenete
            ho <a n:href="Projects:default">zde</a>.
        </span>
        <span class=eng>
            This email is about {$name} (aka {$abbr}) which can
            be found <a n:href="Projects:default">here</a>.
        </span>









SOURCES:
https://phpfashion.com/generovani-odkazu-kuprikladu-v-emailech-a-nette