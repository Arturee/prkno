<?php
namespace App\Model;


/**
 * Object representing row from ad comment table in DB
 */
class AdComment extends Entity {

	/** @var Advertisement */
	private $advertisement;

	/** @var User */
	private $author;

	/** @var string */
	private $content;



	public function __construct(
		EntityMetadata $metadata,
		Advertisement $advertisement,
		User $author,
		string $content
	) {
		parent::__construct($metadata);
		$this->advertisement = $advertisement;
		$this->author = $author;
		$this->content = $content;
	}

	/**
	 * @return
	 */
	public function getAdvertisement(): Advertisement {
		return $this->advertisement;
	}

	/**
	 * @return
	 */
	public function getAuthor(): User {
		return $this->author;
	}

	/**
	 * @return
	 */
	public function getContent(): string {
		return $this->content;
	}
}