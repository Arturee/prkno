<?php
namespace App\Model;

/**
 * Object representing row from document table in DB
 */
class Advertisement extends Entity {

	/** @var User */
	private $author;

	/** @var string */
	private $title;

	/** @var string */
	private $content;

	/** @var string */
	private $type;

	/** @var string */
	private $keywords;	

	/** @var bool */
	private $subscribed;

	/** @var bool */
	private $published;

	public function __construct(
		EntityMetadata $metadata,
		User $author,
		string $title,
		string $content,
		string $type,
		string $keywords,
		bool $subscribed,
		bool $published
	) {
		parent::__construct($metadata);
		$this->author = $author;
		$this->title = $title;
		$this->content = $content;
		$this->type = $type;
		$this->keywords = $keywords;
		$this->subscribed = $subscribed;
		$this->published = $published;
	}

	/**
	 * @return
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return
	 */
	public function setTitle(string $title) {
		return $this->title = $title;
	}

	/**
	 * @return
	 */
	public function getAuthor(): User {
		return $this->author;
	}

	/**
	 * @return
	 */
	public function getContent(): string {
		return $this->content;
	}

	/**
	 * @param $content
 	 */
	public function setContent(string $content) {
		$this->content = $content;
	}

	/**
	 * @return
	 */
	public function getFallbackPath(): string {
		return $this->fallbackPath;
	}

	/**
	 * @param $subscribe
	 */
	public function subscribe(bool $subscribe) {
		$this->subscribed = $subscribe;
	}

	/**
	 * @return
	 */
	public function isSubscribed(): bool {
		return $this->subscribed;
	}
}