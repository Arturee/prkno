<?php
namespace App\Model;


/**
 * Defence attendance entity
 */
class DefenceAttendance extends Entity {

	/** @var User */
	private $user;

	/** @var bool */
	private $vote;

	/** @var string */
	private $comment;

	/** @var bool */
	private $mustCome;

	public function __construct(
		EntityMetadata $metadata,
		User $user,
		bool $vote,
		string $comment,
		bool $mustCome
	) {
		parent::__construct($metadata);
		$this->user = $user;
		$this->vote = $vote;
		$this->comment = $comment;
		$this->mustCome = $mustCome;
	}

	/**
	 * @return
	 */
	public function getUser(): User {
		return $this->user;
	}

	/**
	 * @return
	 */
	public function getVote(): bool {
		return $this->vote;
	}

	/**
	 * @param $vote
	 */
	public function vote(bool $vote) {
		$this->vote = $vote;
	}

	/**
	 * @return
	 */
	public function getComment(): string {
		return $this->comment;
	}

	/**
	 * @return
	 */
	public function mustCome(): bool {
		return $this->mustCome;
	}

	/**
	 * @return 
	 */
	public function setMustCome(bool $mustCome): bool {
		return $this->mustComde = $mustCome;
	}
}