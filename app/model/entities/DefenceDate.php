<?php
namespace App\Model;

use Nette\Utils\DateTime;

/**
 * Object representing row from document table in DB
 */
class DefenceDate extends Entity {

	/** @var DateTime */
	private $date;

	/** @var DateTime */
	private $signupDeadline;

	/** @var bool */
	private $activeVoting;

	public function __construct(
		EntityMetadata $metadata,
		DateTime $date,
		DateTime $signupDeadline,
		bool $activeVote
	) {
		parent::__construct($metadata);
		$this->date = $date;
		$this->signupDeadline = $signupDeadline;
		$this->activeVoting = $activeVote;
	}

	/**
	 * @return
	 */
	public function getDate(): DateTime {
		return $this->date;
	}

	/**
	 * @param $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * @return
	 */
	public function getSignupDeadline(): DateTime {
		return $this->signupDeadline;
	}

	/**
	 * @param $signupDeadline
	 */
	public function setSignupDeadline(DateTime $signupDeadline) {
		$this->signupDeadline = $signupDeadline;
	}

	/**
	 * @return
	 */
	public function isActiveVoting(): bool {
		return $this->activeVoting;
	}

	/**
	 * @param $activeVoting
	 */
	public function setActiveVoting(bool $activeVoting) {
		$this->activeVoting = $activeVoting;
	}
}