<?php
namespace App\Model;


class Document extends Entity {
	/** @var string */
	private $title, $contentMd, $fallbackPath;

	public function __construct(
		EntityMetadata $metadata,
		string $title,
		string $contentMd,
		string $fallbackPath = null
	) {
		parent::__construct($metadata);
		$this->title = $title;
		$this->contentMd = $contentMd;
		$this->fallbackPath = $fallbackPath;
	}


	public function getTitle(): string
    {
		return $this->title;
	}
	public function setTitle(string $title): void
    {
		$this->title = $title;
	}
	public function getContentMd(): string
    {
		return $this->contentMd;
	}
	public function setContentMd(string $contentMd): void
    {
		$this->contentMd = $contentMd;
	}
	public function getFallbackPath(): string
    {
		return $this->fallbackPath;
	}
}