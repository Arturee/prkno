<?php
namespace App\Model;

use Nette;
use Nette\Utils\DateTime;



/**
 * Abstract class representing specific database record
 */
abstract class Entity {
	use Nette\SmartObject;

	/** @var int */
	private $id;
	/** @var DateTime */
	private $updatedAt, $createdAt;


	public function __construct(EntityMetadata $metadata)
    {
		$this->id = $metadata->id;
		$this->createdAt = $metadata->createdAt;
		$this->updatedAt = $metadata->updatedAt;
	}


    public final function getId(): int
    {
        return $this->id;
    }
    public function createdAt(): DateTime
    {
    	return $this->createdAt;
    }
    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }
}