<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 02-Jan-18
 * Time: 8:51 PM
 */

namespace App\Model;

use Nette;
use Nette\Database\IRow;


class EntityMetadata
{
    use Nette\SmartObject;

    public $id;
    public $updatedAt;
    public $createdAt;

    public function __construct(IRow $row)
    {
        $this->id = $row[Repository::COLUMN_ID];
        $this->createdAt = $row[Repository::COLUMN_CREATED_AT];
        $this->updatedAt = $row[Repository::COLUMN_UPDATED_AT];
    }
}