<?php
namespace App\Model;

/**
 * Object representing row from document table in DB
 */
class Keyword extends Entity {

	/** @var string */
	private $word;

	public function __construct(
        EntityMetadata $metadata,
		string $word
	) {
		parent::__construct($metadata);
		$this->word = $word;
	}

	public function getWord(): string
    {
		return $this->word;
	}
}