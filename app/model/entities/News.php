<?php
namespace App\Model;


class News extends Entity {

	/** @var User */
	private $author;
	/** @var bool */
	private $visibleToGuests;
	/** @var Document */
	private $documentCS, $documentEN;
	/** @var bool */
	private $importance;

	public function __construct(
        EntityMetadata $metadata,
		User $author,
		Document $documentCS,
		Document $documentEN,
		bool $importance,
		bool $visibleToGuests
	) {
		parent::__construct($metadata);
		$this->author = $author;
		$this->documentCS = $documentCS;
		$this->documentEN = $documentEN;
		$this->importance = $importance;
		$this->visibleToGuests = $visibleToGuests;
	}



	/**
	 * Get author
	 *
	 * @return
	 */
	public function getAuthor(): ?User {
		return $this->author;
	}

	/**
	 * Get title
	 *
	 * @return Title of the news
	 */
	public function getTitle(bool $isLocaleCs = true): string {
		$document = $this->getDocument($isLocaleCs);
		return $document->getTitle();
	}

	/**
	 * Get czech document
	 *
	 * @return document
	 */
	public function getDocumentCS(): ?Document {
		return $this->documentCS;
	}

	/**
	 * Get english document
	 *
	 * @return document
	 */
	public function getDocumentEN(): ?Document {
		return $this->documentEN;
	}

	/**
	 * Get markdown of the news
	 *
	 * @return Markdown of the news
	 */
	public function getContent(bool $isLocaleCs = true): string {
		$document = $this->getDocument($isLocaleCs);
		return $document->getContentMd();
	}

	/**
	 * @return
	 */
	public function isImportant(): bool {
		return $this->importance;
	}

	/**
	 * @param $important
	 */
	public function setImportant(bool $important) {
		$this->$important = $important;
	}

	/**
	 * @return
	 */
	public function isVisibleToGuests(): bool {
		return $this->visibleToGuests;
	}

	/**
	 * @param $visibleToGuests
	 */
	public function setVisibleToGuests(bool $visibleToGuests) {
		$this->visibleToGuests = $visibleToGuests;
	}

	/**
	 * Return document for language. If not defined, return document
	 * for the other language
	 *
	 * @param $isLocale 	True for czech document, false for the english one
	 * @return Document 	Language specific document object
	 */
	private function getDocument(bool $isLocaleCs = true): Document
    {
		return $isLocaleCs ? $this->documentCS : $this->documentEN;
	}
}