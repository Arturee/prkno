<?php
namespace App\Model;

/**
 * Page entity
 */
class Page extends Entity {

	/** @var int */
	private $order;

	function __construct(
        EntityMetadata $metadata,
		int $order
	) {
		parent::__construct($metadata);
		$this->order = $order;
	}

	/**
	 * Get order. Order numbers of all items of the
	 * table are unique and are numbers from 1 to n,
	 * where n is number of items.
	 *
	 * @return Order of the page
	 */
	public function getOrder(): int {
		return $this->order;
	}
}