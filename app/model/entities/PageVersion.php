<?php
namespace App\Model;


/**
 * Object representing row from page version table in DB
 */
class PageVersion extends Entity {

	/** @var Page */
	private $page;

	/** @var User */
	private $user;

	/** @var Document */
	private $documentCS, $documentEN;

	public function __construct(
        EntityMetadata $metadata,
		Page $page,
		User $user,
		Document $documentCS,
		Document $documentEN
	) {
		parent::__construct($metadata);
		$this->page = $page;
		$this->user = $user;
		$this->documentCS = $documentCS;
		$this->documentEN = $documentEN;
	}

	/**
	 * @return Page entity
	 */
	public function getPage(): Page {
		return $this->page;
	}

	/**
	 * @return User who created this page version
	 */
	public function getCreator(): User {
		return $this->user;
	}

	/**
	 * Get title
	 *
	 * @return Title of the news
	 */
	public function getTitle(bool $isLocaleCs = true): string {
		$document = $this->getDocument($isLocaleCs);
		return $document->getTitle();
	}

	/**
	 * Get czech document
	 *
	 * @return document
	 */
	public function getDocumentCS(): ?Document {
		return $this->documentCS;
	}

	/**
	 * Get english document
	 *
	 * @return document
	 */
	public function getDocumentEN(): ?Document {
		return $this->documentEN;
	}

	/**
	 * Get markdown of the news
	 *
	 * @return Markdown of the news
	 */
	public function getContent(bool $isLocaleCs = true): string {
		$document = $this->getDocument($isLocaleCs);
		return $document->getContentMd();
	}

	/**
	 * Return document for language. If not defined, return document
	 * for the other language
	 *
	 * @param $isLocale 	True for czech document, false for the english one
	 * @return Document 	Language specific document object
	 */
	private function getDocument(bool $isLocaleCs = true): Document {
		if (!$this->documentCS) {
			return $this->documentEN;
		}
		if (!$this->documentEN) {
			return $this->documentCS;
		}
		return $isLocaleCs ? $this->documentCS : $this->documentEN;
	}
}