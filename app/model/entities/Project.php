<?php
namespace App\Model;


use Nette\Utils\DateTime;


class Project extends Entity {

	const
		STATE_PROPOSAL = 'proposal',
		STATE_ACCEPTED = 'accepted',
		STATE_ANALYSIS = 'analysis',
		STATE_ANALYSIS_HANDED_IN = 'analysis_handed_in',
		STATE_IMPLEMENTATION = 'implementation',
		STATE_IMPLEMENTATION_HANDED_IN = 'implementation_handed_in',
		STATE_CONDITIONALLY_DEFENDED = 'conditionally_defended',
		STATE_CONDITIONAL_DOCUMENTS_HANDED_IN = 'conditional_documents_handed_in',
		STATE_DEFENDED = 'defended',
		STATE_FAILED = 'failed';

	/** @var int */
	private $content, $extraCredits;

	/** @var string */
	private $state, $name, $shortName, $description, $keywords;

	/** @var DateTime */
	private $runStartDate, $deadline;

	/** @var bool */
	private $waitingForProposalEvaluation, $requestedToRun, $teamMemberVisible;

	public function __construct(
		EntityMetadata $metadata,
		string $state,
		DateTime $runStartDate = null,
		DateTime $deadline = null,
		string $name,
		string $shortName,
		string $description = null,
		string $content = null,
		bool $waitingForProposalEvaluation = false,
		bool $requestedToRun = false,
		int $extraCredits = null,
		bool $teamMemberVisible = false,
		mixed $keywords = null
	) {
		parent::__construct($metadata);
		$this->content = $content;
		$this->extraCredits = $extraCredits;
		$this->state = $state;
		$this->name = $name;
		$this->shortName = $shortName;
		$this->description = $description;
		$this->keywords = $keywords;
		$this->runStartDate = $runStartDate;
		$this->deadline = $deadline;
		$this->waitingForProposalEvaluation = $waitingForProposalEvaluation;
		$this->requestedToRun = $requestedToRun;
		$this->teamMemberVisible = $teamMemberVisible;
	}

	/**
	 * @return
	 */
	public function getContent(): string {
		return $this->content;
	}

	/**
	 * @param $content
	 */
	public function setContent(string $content) {
		$this->content = $content;
	}

	/**
	 * @return
	 */
	public function getExtraCredits(): int {
		return $this->extraCredits;
	}

	/**
	 * @param $extraCredits
	 */
	public function setExtraCredits(int $extraCredits) {
		return $this->extraCredits;
	}

	/**
	 * @return
	 */
	public function getState(): string {
		return $this->state;
	}

	/**
	 * @param $state
	 */
	public function setState(string $state) {
		return $this->state;
	}

	/**
	 * @return
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param $name
	 */
	public function setName(string $name) {
		return $this->name;
	}

	/**
	 * @return
	 */
	public function getShortName(): string {
		return $this->shortName;
	}

	/**
	 * @param $shortName
	 */
	public function setShortName(string $shortName) {
		$this->shortName = $shortName;
	}

	/**
	 * @return $keywords
	 */
	public function getKeywords(): string {
		return $this->keywords;
	}

	/**
	 * @param $keywords
	 */
	public function setKeywords(string $keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * @return
	 */
	public function getRunStartDate(): DateTime {
		return $this->runStartDate;
	}

	/**
	 * @param
	 */
	public function setRunStartDate(DateTime $runStartDate) {
		$this->runStartDate = $runStartDate;
	}

	/**
	 * @return
	 */
	public function getDeadline(): DateTime {
		return $this->deadline;
	}

	/**
	 * @param $deadline
	 */
	public function setDeadline(DateTime $deadline) {
		$this->deadline = $deadline;
	}

	/**
	 * @return
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return
	 */
	public function isWaitingForProposalEvaluation(): bool {
		return $this->waitingForProposalEvaluation;
	}

	/**
	 * @param $waitingForProposalEvaluation
	 */
	public function setWaitingForProposalEvaluation(bool $waitingForProposalEvaluation) {
		$this->waitingForProposalEvaluation = $waitingForProposalEvaluation;
	}

	/**
	 * @return
	 */
	public function isRequestedToRun(): bool {
		return $this->requestedToRun;
	}

	/**
	 * @param $requestedToRun
	 */
	public function setRequestedToRun(bool $requestedToRun) {
		$this->requestedToRun = $requestedToRun;
	}
	public function isTeamMemberVisible(): bool
    {
		return $this->teamMemberVisible;
	}
	public function setTeamMemberVisible(bool $teamMemberVisible): void
    {
		$this->teamMemberVisible = $teamMemberVisible;
	}
}