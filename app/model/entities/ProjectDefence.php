<?php
namespace App\Model;

use Nette\Utils\DateTime;


/**
 * Object representing row from document table in DB
 */
class ProjectDefence extends Entity {

	/** @var Project */
	private $project;

	/** @var DefenceDate */
	private $defenceDate;

	/** @var User */
	private $opponent;

	/** @var User */
	private $statementAuthor;

	/** @var string */
	private $room;

	/** @var DateTime */
	private $time;

	/** @var Document */
	private $defenceStatement;

	/** @var Document */
	private $opponentReview;

	/** @var Document */
	private $supervisorReview;

	/** @var bool */
	private $defended;

	public function __construct(
        EntityMetadata $metadata,
		Project $project,
		DefenceDate $defenceDate,
		User $opponent,
		User $statementAuthor,
		string $room,
		DateTime $time,
		Document $defenceStatement,
		Document $opponentReview,
		Document $supervisorReview,
		bool $defended
	) {
		parent::__construct($metadata);
		$this->project = $project;
		$this->defenceDate = $defenceDate;
		$this->opponent = $opponent;
		$this->statementAuthor = $statementAuthor;
		$this->room = $room;
		$this->time = $time;
		$this->defenceStatement = $defenceStatement;
		$this->opponentReview = $opponentReview;
		$this->supervisorReview = $supervisorReview;
		$this->defended = $defended;
	}

	/**
	 * @return
	 */
	public function getProject(): Project {
		return $this->project;
	}

	/**
	 * @return
	 */
	public function getDefenceDate(): DefenceDate {
		return $this->defenceDate;
	}

	/**
	 * @return
	 */
	public function getOpponent(): User {
		return $this->opponent;
	}

	/**
	 * @return
	 */
	public function getStatementAuthor(): User {
		return $this->statementAuthor;
	}

	/**
	 * @return
	 */
	public function getRoom(): string {
		return $this->room;
	}

	/**
	 * @return
	 */
	public function getTime(): DateTime {
		return $this->time;
	}

	/**
	 * @return
	 */
	public function getDefenceStatement(): Document {
		return $this->defenceStatement;
	}

	/**
	 * @return
	 */
	public function getOpponentReview(): Document {
		return $this->opponentReview;
	}

	/**
	 * @return 
	 */
	public function getSupervisorReview(): Document {
		return $this->supervisorReview;
	}

	/**
	 * @return
	 */
	public function getDefended(): bool {
		return $this->defended;
	}
}