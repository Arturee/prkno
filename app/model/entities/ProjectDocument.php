<?php
namespace App\Model;

use Nette\Utils\DateTime;



/**
 * Object representing row from document table in DB
 */
class ProjectDocument extends Entity {

	/** @var Project */
	private $project;

	/** @var Document */
	private $document;

	/** @var string */
	private $comment;

	/** @var bool */
	private $private;

	/** @var DateTime */
	private $submitDate;

	/** @var int */
	private $defenceNumber;

	/** @var string */
	private $type;

	/**
	 * Document constructor.
	 *
	 * @param $row
	 * @param $title
	 * @param $contentMd
	 * @param $fallbackPath
	 */
	public function __construct(
        EntityMetadata $metadata,
		Project $project,
		Document $document,
		string $comment,
		bool $private,
		DateTime $submitDate,
		int $defenceNumber,
		string $type
	) {
		parent::__construct($metadata);
		$this->project = $project;
		$this->document = $document;
		$this->comment = $comment;
		$this->private = $private;
		$this->submitDate = $submitDate;
		$this->defenceNumber = $defenceNumber;
		$this->type = $type;
	}

	/**
	 * @return
	 */
	public function getProject(): Project {
		return $this->project;
	}

	/**
	 * @return
	 */
	public function getDocument(): Document {
		return $this->document;
	}

	/**
	 * @return
	 */
	public function getComment(): string {
		return $this->comment;
	}
 
	/**
	 * @return
	 */
	public function isPrivate(): bool {
		return $this->private;
	}

	/**
	 * @return
	 */
	public function getSubmitDate(): DateTime {
		return $this->submitDate;
	}

	/**
	 * @return
	 */
	public function getDefenceNumber(): int {
		return $this->defenceNumber;
	}

	/**
	 * @return
	 */
	public function getType(): string {
		return $this->type;
	}
}