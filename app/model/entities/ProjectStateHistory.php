<?php
namespace App\Model;

use Nette\Utils\DateTime;



/**
 * Object representing row from document table in DB
 */
class ProjectStateHistory extends Entity {

	/** @var string */
	private $state, $comment;
	/** @var DateTime */
	private $validSince;

	public function __construct(
        EntityMetadata $metadata,
		string $state,
		string $comment,
		DateTime $validSince
	) {
		parent::__construct($metadata);
		$this->state = $state;
		$this->comment = $comment;
		$this->validSince = $validSince;
	}

}