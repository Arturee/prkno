<?php
namespace App\Model;

class Proposal extends Entity {

	/** Types of the proposal */
	const
		STATE_DRAFT = 'draft',
		STATE_SENT = 'sent',
		STATE_ACCEPTED = 'accepted',
		STATE_DECLINED = 'declined';

	/** @var Document */
	private $document;
	/** @var string */
	private $state;

	public function __construct(
        EntityMetadata $metadata,
		Document $document,
		string $state
	) {
		parent::__construct($metadata);
		$this->document = $document;
		$this->state = $state;
	}




	public function getDocument(): Document
    {
		return $this->document;
	}
	public function getState(): string
    {
		return $this->state;
	}

	public function setState(string $state): void
    {
		$this->state = $state;
	}
}