<?php
namespace App\Model;



/**
 * Object representing row from document table in DB
 */
class ProposalComment extends Entity {

	/** @var User */
	private $creator;

	/** @var Proposal */
	private $proposal;

	/** @var string */
	private $content;

	/** @var bool */
	private $committeeOnly, $sendEmail;

	public function __construct(
		EntityMetadata $metadata,
		User $creator,
		Proposal $proposal,
		string $content,
		bool $committeeOnly,
		bool $sendEmail
	) {
		parent::__construct($metadata);
		$this->creator = $creator;
		$this->proposal = $proposal;
		$this->content = $content;
		$this->committeeOnly = $committeeOnly;
		$this->sendEmail = $sendEmail;
	}

	/**
	 * @return
	 */
	public function getCreator(): User {
		return $this->creator;
	}

	/**
	 * @return
	 */
	public function getProposal(): Proposal {
		return $this->proposal;
	}

	/**
	 * @return
	 */
	public function getContent(): string {
		return $this->content;
	}

	/**
	 * @return
	 */
	public function getCommitteOnly(): bool
    {
		return $this->committeeOnly;
	}

	/**
	 * @param $committeeOnly
	 */
	public function setCommitteOnly(bool $committeeOnly) {
		$this->committeeOnly = $committeeOnly;
	}

	/**
	 * @return
	 */
	public function sendEmail(): bool {
		return $this->sendEmail;
	}

	/**
	 * @param sendEmail
	 */
	public function setSendEmail(bool $sendEmail)
    {
		$this->sendEmail = $sendEmail;
	}
}