<?php
namespace App\Model;


/**
 * Object representing row from document table in DB
 */
class ProposalVote extends Entity {

	/** @var User */
	private $user;

	/** @var Proposal */
	private $proposal;

	/** @var bool */
	private $vote;

	/**
	 * Document constructor.
	 *
	 * @param $row
	 * @param $user
	 * @param $proposal
	 * @param $vote
	 */
	public function __construct(
		EntityMetadata $metadata,
		User $user,
		Proposal $proposal,
		bool $vote
	) {
		parent::__construct($metadata);
		$this->user = $user;
		$this->proposal = $proposal;
		$this->vote = $vote;
	}

	/**
	 * @return
	 */
	public function getUser(): string {
		return $this->user;
	}

	/**
	 * @return
	 */
	public function getProposal(): string {
		return $this->proposal;
	}

	/**
	 * @return
	 */
	public function getVote(): string {
		return $this->vote;
	}

	/**
	 * @param $vote
	 */
	public function setVote(bool $vote) {
		$this->vote = $vote;
	}
}