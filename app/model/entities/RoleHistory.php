<?php
namespace App\Model;

use Nette\Utils\DateTime;


class RoleHistory extends Entity {

	/** @var User */
	private $user;

	/** @var string */
	private $role;

	/** @var string */
	private $comment;

	/** @var DateTime */
	private $validSince;

	public function __construct(
        EntityMetadata $metadata,
		User $user,
		string $role,
		string $comment,
		DateTime $validSince
	) {
		parent::__construct($metadata);
		$this->user = $user;
		$this->role = $role;
		$this->comment = $comment;
		$this->validSince = $validSince;
	}

	/**
	 * @return
	 */
	public function getUser(): User {
		return $this->title;
	}

	/**
	 * @return
	 */
	public function getRole(): string {
		return $this->role;
	}

	/**
	 * @return
	 */
	public function getComment(): string {
		return $this->comment;
	}

	/**
	 * @return
	 */
	public function validSince(): DateTime {
		return $this->validSince;
	}
}