<?php
namespace App\Model;

use Nette\Utils\DateTime;
use Nette\Security\Passwords;

/**
 * @author Artur
 *
 * UKCO and username are not accessible via CAS, so we won't use them at all.
 */
class User extends Entity {
	/** User roles in system */
	const
		ROLE_GUEST = 'guest', //null is also considered GUEST
		ROLE_STUDENT = 'student',
		ROLE_ACADEMIC = 'academic',
		ROLE_COMMITTEE_MEMBER = 'committee_member',
		ROLE_COMMITTEE_CHAIR = 'committee_chair',
		ROLE_COMMITTEE_SECRETARY = 'committee_secretary';

	/** Login options */
	const
		LOGIN_OPTION_CAS = 'cas',
		LOGIN_OPTION_DB = 'db';


	/** @var string */
	private $name, $degreePrefix, $degreeSuffix, $email, $passwordHash,
        $newsSubscriptionLanguage, $role, $loginOption, $customUrl, $passwordRecoveryToken;

	/** @var DateTime */
	private $lastLogin, $roleExpiration, $lastCasRefresh, $passwordRecoveryTokenExpiration;
    
	/** @var bool */
	private $createdManually, $banned, $roleIsFromCas;

    /**
     * @param $row
     * @param $name
     * @param $email
     * @param $role
     * @param $createdManually
     * @param $banned
     * @param $loginOption
     * @param roleIsFromCas
     * @param $roleIsFromCas
     */
	public function __construct(
        EntityMetadata $metadata,
		string $name,
		string $email,
        string $role,
        bool $createdManually,
        bool $banned,
        string $loginOption,
		bool $roleIsFromCas,

		?string $passwordHash,
		?string $newsSubscriptionLanguage,
        ?DateTime $lastLogin,
        ?DateTime $roleExpiration,
        ?string $degreePrefix,
        ?string $degreeSuffix,
        ?string $customUrl,
        ?DateTime $lastCasRefresh,
        ?string $passwordRecoveryToken,
        ?DateTime $passwordRecoveryTokenExpiration
	) {
		parent::__construct($metadata);
		$this->name = $name;
        $this->email = $email;
        $this->role = $role;
        $this->createdManually = $createdManually;
        $this->banned = $banned;
        $this->loginOption = $loginOption;
        $this->roleIsFromCas = $roleIsFromCas;
        $this->passwordHash = $passwordHash;
        $this->newsSubscriptionLanguage = $newsSubscriptionLanguage;
        $this->lastLogin = $lastLogin;
        $this->roleExpiration = $roleExpiration;
		$this->degreePrefix = $degreePrefix;
		$this->degreeSuffix = $degreeSuffix;
		$this->customUrl = $customUrl;
		$this->lastCasRefresh = $lastCasRefresh;
        $this->passwordRecoveryToken = $passwordRecoveryToken;
        $this->passwordRecoveryTokenExpiration = $passwordRecoveryTokenExpiration;
	}

    /**
     * @return
     */
	public function getName() : string {
		return $this->name;
	}

    /**
     * @param $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @return
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail(string $email) {
        $this->email = $email;
    }

    /**
     * @return
     */
    public function getRole(): string {
        return $this->role;
    }

    /**
     * @param $role
     */
    public function setRole(string $role) {
        $this->role = $role;
    }

    /**
     * 
     */
    public function getLoginOption() : string {
        return $this->loginOption;
    }

    /**
     * @param $loginOption
     */
    public function setLoginOption(string $loginOption) {
        $this->loginOption = $loginOption;
    }

    /**
     * @return
     */
    public function isLoggedInViaCas() : bool {
        return $this->getLoginOption() === self::LOGIN_OPTION_CAS;
    }

    /**
     * @return
     */
    public function isCreatedManually() : bool {
        return $this->createdManually;
    }

    /**
     * @param
     */
    public function setCreatedManually(bool $createdManually) {
        $this->createdManually = $createdManually;
    }

    /**
     * @return
     */
    public function isBanned() : bool {
        return $this->banned;
    }

    /**
     * @param $banned
     */
    public function setBanned(bool $banned) {
        $this->banned = $banned;
    }
    
    /**
     * @return
     */
    public function isRoleFromCas() : bool {
        return $this->roleIsFromCas;
    }

    /**
     * @param $roleFromCas
     */
    public function setRoleFromCas(bool $roleFromCas) {
        $this->$roleFromCas = $roleFromCas;
    }

    /**
     * @return
     */
    public function getLastLogin() : ?DateTime {
        return $this->lastLogin;
    }

    /**
     * @param $lastLogin
     */
    public function setLastLogin(DateTime $lastLogin) {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return
     */
    public function getDegreePrefix(): ?string {
		return $this->degreePrefix;
	}

    /**
     * @param $degreePrefix
     */
    public function setDegreePrefix(DateTime $degreePrefix) {
        $this->degreePrefix = $degreePrefix;
    }
	
    /**
     * @return
     */
    public function getDegreeSuffix(): ?string {
		return $this->degreeSuffix;
	}

    /**
     * @param $degreeSuffix
     */
    public function setDegreeSuffix(DateTime $degreeSuffix) {
        $this->degreeSuffix = $degreeSuffix;
    }
    
    /**
     * @return
     */
    public function getPasswordHash(): ?string {
        return $this->passwordHash;
    }


    public function setPassword(string $password) {
        $passwordHash = Passwords::hash($password);
    }

    /**
     * @return
     */
	public function getNewsSubscriptionLanguage() : ?string {
		return $this->newsSubscriptionLanguage;
	}

    /**
     * @param $newsSubscriptionLanguage
     */
    public function setNewsSubscriptionLanguage(string $newsSubscriptionLanguage) {
        $this->$newsSubscriptionLanguage = $newsSubscriptionLanguage;
    }

    /**
     * @return
     */
    public function getCustomUrl() : ?string {
        return $this->customUrl;
    }

    /**
     * @param $customUrl
     */
    public function setCustomUrl(string $customUrl) {
        $this->customUrl = $customUrl;
    }

    /**
     * @return
     */
    public function getLastCasRefresh() : ?DateTime {
        return $this->lastCasRefresh;
    }

    /**
     * @param $lastCasRefresh
     */
    public function setLastCasRefresh($lastCasRefresh) {
        $this->lastCasRefresh = $lastCasRefresh;
    }

    /**
     * @return
     */
    public function destroyPasswordHash() : void {
        $this->passwordHash = null;
    }

    /**
     * @return 
     */
    public function getPasswordRecoveryToken(): string {
        return $this->passwordRecoveryToken;
    }

    /**
     * @param $passwordRecoveryToken
     */
    public function setPasswordRecoveryToken(string $passwordRecoveryToken) {
        $this->passwordRecoveryToken = $passwordRecoveryToken;
    }

    /**
     * @return
     */
    public function getPasswordRecoveryTokenExpiration(): DateTime {
        return $this->passwordRecoveryTokenExpiration;
    }

    /**
     * @param $passwordRecoveryTokenExpiration
     */
    public function setPasswordRecoveryTokenExpiration(DateTime $passwordRecoveryTokenExpiration) {
        $this->passwordRecoveryTokenExpiration = $passwordRecoveryTokenExpiration;
    }
}