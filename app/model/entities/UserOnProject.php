<?php
namespace App\Model;



class UserOnProject extends Entity {

	/** User on project roles */
	const
		PROJECT_ROLE_TEAM_MEMBER = 'team_member',
		PROJECT_ROLE_SUPERVISOR = 'supervisor',
		PROJECT_ROLE_OPPONENT = 'opponent',
		PROJECT_ROLE_CONSULTANT = 'consultant';

	/** @var User */
	private $user;
	/** @var Project */
	private $project;
	/** @var string */
	private $projectRole, $comment;

	public function __construct(
        EntityMetadata $metadata,
		User $user,
		Project $project,
		string $projectRole,
		string $comment = null
	) {
		parent::__construct($metadata);
		$this->user = $user;
		$this->project = $project;
		$this->projectRole = $projectRole;
		$this->comment = $comment;
	}

	/**
	 * @return
	 */
	public function getUser(): User {
		return $this->user;
	}

	/**
	 * @return
	 */
	public function getProject(): Project {
		return $this->project;
	}

	/**
	 * @return
	 */
	public function getProjectRole(): string {
		return $this->projectRole;
	}

	/**
	 * @param $projectRole
	 */
	public function setProjectRole(string $projectRole) {
		$this->projectRole = $projectRole;
	}

	/**
	 * @return
	 */
	public function getComment(): string {
		return $this->comment;
	}

	/**
	 * @param $comment
	 */
	public function setComment(string $comment) {
		$this->comment = $comment;
	}
}