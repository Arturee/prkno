<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;



/**
 * Represents class managing documents in database
 */
class AdCommentRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'ad_comments',
		COLUMN_AD_ID = 'ad_id',
		COLUMN_USER_ID = 'user_id',
		COLUMN_CONTENT = 'content';

	/** @var AdvertisementRepository */
	private $advertisementRepository;

	/** @var UserRepository */
	private $userRepository;

	public function __construct(
		Context $database,
		DocumentRepository $advertisementRepository,
		AdvertisementRepository $userRepository
	) {
		parent::__construct($database);
		$this->advertisementRepository = $advertisementRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * Insert new ad comment
	 *
	 * @param $adId     Ad Id
	 * @param $userId   User Id
	 * @param $conetnt 	Conetnt
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertAdComment(int $adId, int $userId, string $content): int
    {
		return parent::insert([
			self::COLUMN_AD_ID => $adId,
			self::COLUMN_USER_ID => $userId,
			self::COLUMN_CONTENT => $content
		]);
	}


	/**
	 * @param $adCocument 	Ad comment entity
	 */
	protected function convertEntityToArray($adComment): array
    {
	    throw new NotImplementedError('update mapper');
		return [];
	}
	protected function convertRowToEntity(IRow $row): AdComment
    {
        /** @var Advertisement $ad */
		$ad = $this->advertisementRepository->getById($row[self::COLUMN_AD_ID]);
		/** @var User $user */
		$user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		return new AdComment(
		    new EntityMetadata($row),
			$ad,
			$user,
			$row[self::COLUMN_CONTENT]
		);
	}
}