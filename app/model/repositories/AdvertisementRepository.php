<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class AdvertisementRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'advertisements',
		COLUMN_USER_ID = 'title',
		COLUMN_TITLE = 'content_md',
		COLUMN_CONTENT = 'fallback_path',
		COLUMN_TYPE = 'type',
		COLUMN_KEYWORDS = 'keywords',
		COLUMN_SUBSCRIBED_VIA_EMAIL = 'subscribed_via_email',
		COLUMN_PUBLISHED = 'published';

	/** @var UserRepository */
	private $userRepository;

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     		Database dependency injection
	 * @param $userRepository 		User repository dependency injection
	 * @param $documentRepository 	Document repository
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
	}

	/**
	 * Insert new document into database
	 *
	 * @return $userID 		ID of the creator
	 * @param $title        Title of the advertisement
	 * @param $conentMd     Markdown of the content
	 * @param $type 		Type of the advertisement
	 * @param $keywords 	Keywords
	 * @param $subscribed 	Subscribtion
	 * @param $published 	Whether the advertisement was published
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertAdvertisement(
		int $userId,
		string $title,
		string $contentMd,
		string $type,
		string $keywords,
		string $subscribed,
		bool $published
	): int {
		return parent::insert([
			self::COLUMN_USER_ID => 'title',
			self::COLUMN_TITLE => 'content_md',
			self::COLUMN_CONTENT => 'fallback_path',
			self::COLUMN_TYPE => 'type',
			self::COLUMN_KEYWORDS => 'keywords',
			self::COLUMN_SUBSCRIBED_VIA_EMAIL => 'subscribed_via_email',
			self::COLUMN_PUBLISHED => 'published'
		]);
	}

	/**
	 * @param $advertisement  	Advertisement entity
	 * @return array of updatable columns as key-value pairs
	 */
	protected function convertEntityToArray($advertisement): array {
	    $fallbackPath = '';
	    throw new NotImplementedError('um fallback path');
		return [
			self::COLUMN_TITLE => $advertisement->getTitle(),
			self::COLUMN_CONTENT_MD => $advertisement->getContent(),
			self::COLUMN_SUBSCRIBED_VIA_EMAIL => $advertisement->isSubscribed(),
			self::COLUMN_PUBLISHED => $fallbackPath
		];
	}

	/**
	 * Create Advertisement object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): Document {
	    throw new NotImplementedError('');
		$author = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		return new Document(
		    new EntityMetadata($row),
			$author,
			$row[self::COLUMN_TITLE],
			$row[self::COLUMN_CONTENT],
			$row[self::COLUMN_TYPE],
			$row[self::COLUMN_KEYWORDS],
			$row[self::COLUMN_SUBSCRIBED_VIA_EMAIL],
			$row[self::COLUMN_PUBLISHED]
		);
	}
}