<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class DefenceAttendanceRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'proposal_votes',
		COLUMN_USER_ID = 'user_id',
		COLUMN_VOTE = 'vote',
		COLUMN_COMMENT = 'comment',
		COLUMN_MUST_COME = 'must_come';

	/** @var UserRepository */
	private $userRepository;

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     	Database dependency injection
	 * @param $userRepository 	User repository
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
	}

	/**
	 * Insert new project document into database
	 *
	 * @param $userId 	Id of the user
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertDefenceAttendance(int $userId): int
    {
		return parent::insert([
			self::COLUMN_USER_ID => $userId
		]);
	}

	/**
	 * @param $defenceAttendance 	Defence attendance entity
	 * @return array of updatable columns as key-value pairs
	 */
	protected function convertEntityToArray($defenceAttendance): array
    {
	    /** @var DefenceAttendance $defenceAttendance */
		return [
			self::COLUMN_VOTE => $defenceAttendance->getVote(),
			self::COLUMN_COMMENT => $defenceAttendance->getComment(),
			self::COLUMN_MUST_COME => $defenceAttendance->mustCome()
		];
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): DefenceAttendance
    {
        throw new NotImplementedError('em');
//		$author = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
//		return new Document($row,
//			$author,
//			$row[self::COLUMN_VOTE],
//			$row[self::COLUMN_COMMENT],
//			$row[self::COLUMN_MUST_COME]
//		);
	}
}