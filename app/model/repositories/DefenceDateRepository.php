<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Utils\DateTime;

/**
 * Represents class managing documents in database
 */
class DefenceDateRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'id',
		COLUMN_DATE = 'date',
		COLUMN_SIGNUP_DEADLINE = 'signup_deadline',
		COLUMN_ACTIVE_VOTING = 'active_voting';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database) {
		parent::__construct($database);
	}

	/**
	 * Insert new document into database
	 *
	 * @param $title        Title of the document
	 * @param $conentMd     Markdown of the content
	 * @param $fallbackPath Path to the generated PDF document
	 * @return ID of the inserted row
	 */
	public function insertDefenceDate(DateTime $date, DateTime $signupDeadline, bool $activeVoting): int
    {
		return parent::insert([
			self::COLUMN_DATE => $date,
			self::COLUMN_SIGNUP_DEADLINE => $signupDeadline,
			self::COLUMN_ACTIVE_VOTING => $activeVoting
		]);
	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($defenceDate): array
    {
        throw new NotImplementedError('um');
	}

	/**
	 * Create DefenceDate object from database row
	 * 
	 * @param $row  		Database row
	 * @return Defence date
	 */
	protected function convertRowToEntity(IRow $row): DefenceDate
    {
        throw new NotImplementedError('em');
    }
}