<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class DocumentRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'documents',

		COLUMN_TITLE = 'title',
		COLUMN_CONTENT_MD = 'content_md',
		COLUMN_FALLBACK_PATH = 'fallback_path';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database)
    {
		parent::__construct($database);
	}






    /**
     * @param Document $document
     */
    protected function convertEntityToArray($document): array
    {
        /** @var Document $document */
        // generate
        $fallbackPath = null;
        return [
            self::COLUMN_TITLE => $document->getTitle(),
            self::COLUMN_CONTENT_MD => $document->getContentMd(),
            self::COLUMN_FALLBACK_PATH => $fallbackPath
        ];
    }
    protected function convertRowToEntity(IRow $row): Document
    {
        return new Document(
            new EntityMetadata($row),
            $row[self::COLUMN_TITLE],
            $row[self::COLUMN_CONTENT_MD],
            $row[self::COLUMN_FALLBACK_PATH]
        );
    }








	/**
	 * Insert new document into database
	 *
	 * @param $title        Title of the document
	 * @param $conentMd     Markdown of the content
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertDocument(string $title, string $contentMd): int
    {
		$fallbackPath = null;
		return parent::insert([
			self::COLUMN_TITLE => $title,
			self::COLUMN_CONTENT_MD => $contentMd,
			self::COLUMN_FALLBACK_PATH => $fallbackPath
		]);
	}
}