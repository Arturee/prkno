<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class KeywordRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'keywords',
		COLUMN_WORD = 'word';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database) {
		parent::__construct($database);
	}

	/**
	 * Insert new project document into database
	 * @param $keyword 	Keyword
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertKeyword(strig $keyword): int {
		return parent::insert([
			self::COLUMN_WORD => $keyword
		]);
	}


	/**
	 * @param $document
     *
     * @Override
	 */
	protected function convertEntityToArray($document): array
    {
        throw new NotImplementedError('em');
		return [];
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
     *
     * @Override
	 */
	protected function convertRowToEntity(IRow $row): Document
    {
		throw new NotImplementedError('em');
	}
}