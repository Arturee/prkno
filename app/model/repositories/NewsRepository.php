<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;



/**
 * Class managing news
 */
class NewsRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'news',
		COLUMN_USER_ID = 'user_id',
		COLUMN_DOCUMENT_ID_CS = 'document_id_cs',
		COLUMN_DOCUMENT_ID_EN = 'document_id_en',
		COLUMN_IMPORTANCE = 'importance',
		COLUMN_GUEST_PUBLIC = 'guest_public';

	/** @var DocumentRepository */
	private $documentRepository;

	/** @var UserRepository */
	private $userRepository;

	/**
	 * Create new NewsRepository object
	 *
	 * @param $database     Database dependency injection
	 */
	public function __construct(
		Context $database,
		DocumentRepository $documentRepository,
		UserRepository $userRepository
	) {
		parent::__construct($database);
		$this->documentRepository = $documentRepository;
		$this->userRepository = $userRepository;
	}







    /**
     * @Override
     * @param News $news
     * @return array
     */
    protected function convertEntityToArray($news): array
    {
        $this->documentRepository->update($news->getDocumentCS());
        $this->documentRepository->update($news->getDocumentEN());
        return [
            self::COLUMN_IMPORTANCE => $news->isImportant(),
            self::COLUMN_GUEST_PUBLIC => $news->isVisibleToGuests(),
        ];
    }
    /**
     * @Override
     * @return News
     */
    protected function convertRowToEntity(IRow $row): Entity
    {
        /**
         * @var User $user
         * @var Document $documentCS
         * @var Document $documentEN
         */
        $user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
        $documentCS = $this->documentRepository->getById($row[self::COLUMN_DOCUMENT_ID_CS]);
        $documentEN = $this->documentRepository->getById($row[self::COLUMN_DOCUMENT_ID_EN]);
        return new News(
            new EntityMetadata($row),
            $user,
            $documentCS,
            $documentEN,
            $row[self::COLUMN_IMPORTANCE],
            $row[self::COLUMN_GUEST_PUBLIC]
        );
    }


    /**
     * @param int $userID
     * @param string $titleCS
     * @param string $contentCS
     * @param string $titleEN
     * @param string $contentEN
     * @param bool $isImportant
     * @param bool $guestPublic
     * @return int ID of the newly inserted news
     */
	public function insertNews(
		int $authorID,
		string $titleCS,
		string $contentCS,
		string $titleEN,
		string $contentEN,
		bool $isImportant = false,
		bool $guestPublic = false
	): int
    {
		$contentCSid = $this->documentRepository->insertDocument($titleCS, $contentCS);
		$contentENid = $this->documentRepository->insertDocument($titleEN, $contentEN);
		return parent::insert([
			self::COLUMN_USER_ID => $authorID,
			self::COLUMN_DOCUMENT_ID_CS => $contentCSid,
			self::COLUMN_DOCUMENT_ID_EN => $contentENid,
			self::COLUMN_IMPORTANCE => $isImportant,
			self::COLUMN_GUEST_PUBLIC => $guestPublic
		]);
	}

	/**
	 * Delete news with documents attached to it
	 *
	 * @param $id   ID of the news
	 */
	public function deleteById(int $id): void
    {
        /** @var News $news */
		$news = $this->getById($id);
		$documentCS = $news->getDocumentCS();
		$documentEN = $news->getDocumentEN();
		parent::deleteById($id);
		$this->documentRepository->deleteEntity($documentCS);
		$this->documentRepository->deleteEntity($documentEN);
	}
}