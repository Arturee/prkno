<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class PageRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'page',
		COLUMN_PAGE_ORDER = 'page_order';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(
		Context $database
	) {
		parent::__construct($database);
	}

	/**
	 * Insert new page into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertPage(): int {
		$orders = $this->getAllFromColumn(self::COLUMN_PAGE_ORDER);
		$order = sizeof($orders) > 0 ? max($orders) + 1 : 1;
		return parent::insert([
			self::COLUMN_PAGE_ORDER => $order
		]);
	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($page): array
    {
	    throw new NotImplementedError('update mapper');
	}
	/**
	 * @return Page
	 */
	protected function convertRowToEntity(IRow $row): Entity
    {
		return new Page(
            new EntityMetadata($row),
			$row[self::COLUMN_PAGE_ORDER]
		);
	}
}