<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\Table\Selection;

/**
 * Abstract class. All Repositories have to extends from it.
 */
class PageVersionRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'page_versions',
		COLUMN_PAGE_ID = 'page_id',
		COLUMN_USER_ID = 'user_id',
		COLUMN_DOCUMENT_ID_CS = 'document_id_cs',
		COLUMN_DOCUMENT_ID_EN = 'document_id_en';

	/** @var UserRepository */
	private $userRepository;

	/** @var PageRepository */
	private $pageRepository;

	/** @var DocumentRepository */
	private $documentRepository;

	/**
	 * Create new instance of the user.
	 *
	 * @param $database 	Dependency injection with database
	 * @param $tableName 	Table name
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository,
		PageRepository $pageRepository,
		DocumentRepository $documentRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
		$this->pageRepository = $pageRepository;
		$this->documentRepository = $documentRepository;
	}

	/**
	 * Insert new document into database
	 *
	 * @param $titleCS 	Title of the document
	 * @param $conentCS Markdown of the content
	 * @param $titleEN  Title of the document
	 * @param $conentEN Markdown of the content
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertPageVersion(string $titleCS, string $contentCS, string $titleEN, string $contentEN, int $userId, int $pageId = null): int {
		if (!$titleCS || !$contentCS || !$titleCS || !$contentEN) {
			throw new Exception("Title and content for both languages has to be defined.");
			//TODO use specifie exc
		}
		if ($pageId == null) {
			$pageId = $this->pageRepository->insertPage();
		}
		$contentCSid = $this->documentRepository->insertDocument($titleCS, $contentCS);
		$contentENid = $this->documentRepository->insertDocument($titleEN, $contentEN);
		return parent::insert([
			self::COLUMN_PAGE_ID => $pageId,
			self::COLUMN_USER_ID => $userId,
			self::COLUMN_DOCUMENT_ID_CS => $contentCSid,
			self::COLUMN_DOCUMENT_ID_EN => $contentENid
		]);
	}

	/**
	 * Delete page version with documents attached to it
	 *
	 * @param $id   ID of the page version
	 */
	public function deleteById(int $id): void
    {
	    /** @var PageVersion $pageVersion */
		$pageVersion = $this->getById($id);
		$page = $pageVersion->getPage();
		$documentCS = $pageVersion->getDocumentCS();
		$documentEN = $pageVersion->getDocumentEN();

		//TODO - delete also page, if no page versions are there

		parent::deleteEntity($pageVersion);
		$this->documentRepository->deleteEntity($documentCS);
		$this->documentRepository->deleteEntity($documentEN);
	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($pageVersion): array {
		throw new NotImplementedError('um');
		
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): PageVersion {
		$user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		$page = $this->pageRepository->getById($row[self::COLUMN_PAGE_ID]);
		$documentCS = $this->documentRepository->getById($row[self::COLUMN_DOCUMENT_ID_CS]);
		$documentEN = $this->documentRepository->getById($row[self::COLUMN_DOCUMENT_ID_EN]);
		return new PageVersion(
            new EntityMetadata($row),
			$page,
			$user,
			$documentCS,
			$documentEN
		);
	}
}