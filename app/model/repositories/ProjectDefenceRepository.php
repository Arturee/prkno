<?php
namespace App\Model;

use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class ProjectDefenceRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'project_defence',
		COLUMN_PROJECT_ID = 'project_id',
		COLUMN_DEFENCE_DATE_ID = 'defence_date_id',
		COLUMN_OPPONENT_ID = 'opponent_id',
		COLUMN_STATEMENT_AUTHOR_ID = 'statement_author_id',
		COLUMN_ROOM = 'room',
		COLUMN_TIME = 'time',
		COLUMN_DEFENCE_STATEMENT = 'defence_statement',
		COLUMN_OPPONENT_REVIEW = 'opponent_review',
		COLUMN_SUPERVISOR_REVIEW = 'supervisor_review',
		COLUMN_DEFENDED = 'defended';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database) {
		parent::__construct($database);
	}

	/**
	 * Insert new document into database
	 *
	 * @param $title        Title of the document
	 * @param $conentMd     Markdown of the content
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertDocument(): int {

	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($document): array {
		//TODO implement
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): Document {
		//TODO implement


	}
}