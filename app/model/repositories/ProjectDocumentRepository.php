<?php
namespace App\Model;

use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class ProjectDocumentRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'project_document',
		COLUMN_PROJECT_ID = 'project_id',
		COLUMN_DOCUMENT_ID = 'document_id',
		COLUMN_COMMENT = 'comment',
		COLUMN_TEAM_PRIVATE = 'team_private',
		COLUMN_DATE_SUBMITTED = 'date_submitted',
		COLUMN_DEFENCE_NUMBER = 'defence_number',
		COLUMN_TYPE = 'type';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database) {
		parent::__construct($database);
	}

	/**
	 * Insert new project document into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertProjectDocument(): int {

	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($document): array {
		//TODO implement
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): Document {
		//TODO implement


	}
}