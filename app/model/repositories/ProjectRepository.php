<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\UniqueConstraintViolationException;
use App\Model\UserOnProjectRepository;

/**
 * Class managing projects
 */
class ProjectRepository extends Repository {
	use Nette\SmartObject;

	/** column names */
	const
		TABLE_NAME = 'projects',
		COLUMN_STATE = 'state',
		COLUMN_RUN_START_DATE = 'run_start_date',
		COLUMN_DEADLINE = 'deadline',
		COLUMN_NAME = 'name',
		COLUMN_SHORT_NAME = 'short_name',
		COLUMN_DESCRIPTION = 'description',
		COLUMN_CONTENT = 'content',
		COLUMN_WAITING_FOR_PROPOSAL_EVALUATION = 'waiting_for_proposal_evaluation',
		COLUMN_REQUESTED_TO_RUN = 'requested_to_run',
		COLUMN_EXTRA_CREDITS= 'extra_credits',
		COLUMN_TEAM_MEMBERS_VISIBLE = 'team_members_visible',
		COLUMN_KEYWORDS = 'keywords';

	/** @var ProposalRepository */
	private $proposalRepository;

	public function __construct(
		Context $database,
		ProposalRepository $proposalRepository
	) {
		parent::__construct($database);
		$this->proposalRepository = $proposalRepository;
	}









    /**
     * @Override
     * @param Project $project
     */
    protected function convertEntityToArray($project): array
    {
        /** @var Project $project */
        return [
            self::COLUMN_STATE => $project->getState(),
            self::COLUMN_RUN_START_DATE => $project->getRunStartDate(),
            self::COLUMN_DEADLINE => $project->getDeadline(),
            self::COLUMN_NAME => $project->getName(),
            self::COLUMN_SHORT_NAME => $project->getShortName(),
            self::COLUMN_DESCRIPTION => $project->getDescription(),
            self::COLUMN_CONTENT => $project->getContent(),
            self::COLUMN_WAITING_FOR_PROPOSAL_EVALUATION => $project->isWaitingForProposalEvaluation(),
            self::COLUMN_REQUESTED_TO_RUN => $project->isRequestedToRun(),
            self::COLUMN_EXTRA_CREDITS => $project->getExtraCredits(),
            self::COLUMN_TEAM_MEMBERS_VISIBLE => $project->isTeamMemberVisible(),
            self::COLUMN_KEYWORDS => $project->getKeywords()
        ];
    }
    /**
     * @Override
     */
    protected function convertRowToEntity(IRow $row): Project
    {
        return new Project(
            new EntityMetadata($row),
            $row[self::COLUMN_STATE],
            $row[self::COLUMN_RUN_START_DATE],
            $row[self::COLUMN_DEADLINE],
            $row[self::COLUMN_NAME],
            $row[self::COLUMN_SHORT_NAME],
            $row[self::COLUMN_DESCRIPTION],
            $row[self::COLUMN_CONTENT],
            $row[self::COLUMN_WAITING_FOR_PROPOSAL_EVALUATION],
            $row[self::COLUMN_REQUESTED_TO_RUN],
            $row[self::COLUMN_EXTRA_CREDITS],
            $row[self::COLUMN_TEAM_MEMBERS_VISIBLE],
            $row[self::COLUMN_KEYWORDS]
        );
    }








	/**
	 * Insert new project into database
	 *
	 * @param $proposal 	Proposal used for project
	 * @param $name         Name of the project
	 * @param $shortName    Short name (acronym)
	 * @param $description  Description of the project in markdown
	 * @return ID of the inserted row
     *
	 * @throws UniqueConstraintViolationException
	 */
	public function insertProject(string $name, string $shortName, string $description): int
    {
		$projectId = $this->insert([
			self::COLUMN_NAME => $name,
			self::COLUMN_SHORT_NAME => $shortName,
			self::COLUMN_DESCRIPTION => $description,
			self::COLUMN_STATE => Project::STATE_PROPOSAL
		]);
		$this->proposalRepository->insertProposal($projectId, $name, $description);
		return $projectId;
	}
	public function deleteById(int $id): void
    {
		$this->proposalRepository->deleteFromProject($id);
		//TODO name the funciton correctly
		// remove all proposal for that project
		parent::deleteById($id);
	}





	public function insertNewProposal(int $projectId, string $name, string $description): int
    {
        throw new NotImplementedError('what is this?');
		$this->proposalRepository->insertProposal($projectId, $name, $description);
	}

	public function getProjectByShortName(string $shortName): ?Project
    {
        /** @var Project $result */
        $result = parent::getByColumn(self::COLUMN_SHORT_NAME, $shortName);
		return $result;
	}
}