<?php
namespace App\Model;

use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class ProjectStateRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'project_state_history',
		COLUMN_PROJECT_ID = 'project_id',
		COLUMN_STATE = 'state',
		COLUMN_COMMENT = 'comment',
		COLUMN_VALID_SINCE = 'valid_since';

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(Context $database) {
		parent::__construct($database);
	}

	/**
	 * Insert new project document into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertProjectDocument(): int {
		//TODO implement


	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($document): array {
		//TODO implement


	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): Document {
		//TODO implement


	}
}