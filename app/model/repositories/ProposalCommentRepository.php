<?php
namespace App\Model;

use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class ProposalCommentRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'proposal_comment',
		COLUMN_CREATOR_ID = 'creator_id',
		COLUMN_PROPOSAL_ID = 'proposal_id',
		COLUMN_CONTENT = 'content',
		COLUMN_COMMITTEE_ONLY = 'committee_only',
		COLUMN_SEND_EMAIL = 'send_email';

	/** @var UserRepository */
	private $userRepository;

	/** @var ProposalRepository */
	private $proposalRepository;

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository,
		ProposalRepository $proposalRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
		$this->proposalRepository = $proposalRepository;
	}

	/**
	 * Insert new project document into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertProposalComment(int $userId, int $proposalId, string $comment,
                                          bool $committeOnly = false, bool $sendEmail = false): int
    {
		return parent::insert([
			self::COLUMN_CREATOR_ID => $userId,
			self::COLUMN_PROPOSAL_ID => $proposalId,
			self::COLUMN_CONTENT => $comment,
			self::COLUMN_COMMITTEE_ONLY => $committeOnly,
			self::COLUMN_SEND_EMAIL => $sendEmail
		]);
	}

	/**
	 *
	 * @param $document
	 */
	protected function convertEntityToArray($proposalComment): array
    {
        /** @var ProposalComment $proposalComment */
		return [
			self::COLUMN_COMMITTEE_ONLY => $proposalComment->getContent(),
			self::COLUMN_SEND_EMAIL => $proposalComment->sendEmail()
		];
	}

	/**
	 * Create ProposalComment object from database row
	 * 
	 * @param $row  Database row
	 * @return ProposalComment
	 */
	protected function convertRowToEntity(IRow $row): ProposalComment
    {
	    /**
         * @var User $creator
         * @var Proposal $proposal
         */
		$creator = $this->userRepository->getById($row[self::COLUMN_CREATOR_ID]);
		$proposal = $this->proposalRepository->getById($row[self::COLUMN_PROPOSAL_ID]);
		return new ProposalComment(
		    new EntityMetadata($row),
			$creator,
			$proposal,
			$row[self::COLUMN_CONTENT],
			$row[self::COLUMN_COMMITTEE_ONLY],
			$row[self::COLUMN_SEND_EMAIL]
		);
	}
}