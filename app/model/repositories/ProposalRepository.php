<?php
namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\UniqueConstraintViolationException;




/**
 * Repository for proposals
 */
class ProposalRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'proposals',
		COLUMN_DOCUMENT_ID = 'document_id',
		COLUMN_PROJECT_ID = 'project_id',
		COLUMN_STATE = 'state';

	/** @var DocumentRepository */
	private $documentRepository;


	public function __construct(
		Context $database,
		DocumentRepository $documentRepository
	) {
		parent::__construct($database);
		$this->documentRepository = $documentRepository;
	}

	/**
	 * Insert proposal. This method is called by project repository
	 *
	 * @param $title 		Title
	 * @param $description 	Description
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertProposal(int $projectId, string $title, string $description) : int
    {
		$documentId = $this->documentRepository->insertDocument($title, $description);
		return parent::insert([
			self::COLUMN_DOCUMENT_ID => $documentId,
			self::COLUMN_PROJECT_ID => $projectId,
			self::COLUMN_STATE => Proposal::STATE_DRAFT
		]);
	}
    /**
     * @param int $projectId
     * @return Proposal[]
     */
	public function getProposals(int $projectId) : array
    {
        $result = $this->table()->where(self::COLUMN_PROJECT_ID, $projectId);
		return $result;
	}
	/**
	 * @param $projectId
	 */
	public function deleteFromProject(int $projectId) : void
    {
		$this->table()
			->where(self::COLUMN_PROJECT_ID, $projectId)
			->delete();
	}
	/**
	 * Delete proposal with document and project
	 *
	 * @param $id
	 */
	public function deleteById(int $id) : void
    {
        /** @var Proposal $proposal */
		$proposal = $this->getById($id);
		$document = $proposal->getDocument();
		parent::deleteById($id);
		$this->documentRepository->deleteEntity($document);
	}
    /**
     * @param Proposal $proposal
     * @return array
     */
	protected function convertEntityToArray($proposal) : array
    {
		$this->documentRepository->update($proposal->getDocument());
		return [
			self::COLUMN_STATE => $proposal->getState()
		];
	}
	/**
	 * Create proposal from database row data
	 *
	 * @param $row  Proposal from database as table row
	 * @return Proposal
	 */
	protected function convertRowToEntity(IRow $row) : Entity
    {
        /** @var Document $document */
		$document = $this->documentRepository->getById($row[self::COLUMN_DOCUMENT_ID]);
		return new Proposal(
            new EntityMetadata($row),
			$document,
			$row[self::COLUMN_STATE]
		);
	}
}