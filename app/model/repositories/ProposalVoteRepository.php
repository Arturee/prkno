<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class ProposalVoteRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'proposal_votes',
		COLUMN_USER_ID = 'user_id',
		COLUMN_PROPOSAL_ID = 'proposal_id',
		COLUMN_VOTE = 'vote';

	/** @var UserRepository */
	private $userRepository;

	/** @var ProposalRepository */
	private $proposalRepository;

	/**
	 * UserOnProjectRepository constructor.
	 *
	 * @param $database         Database
	 * @param $userRepository   User repository
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository,
		ProposalRepository $proposalRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
		$this->proposalRepository = $proposalRepository;
	}

	/**
	 * Insert new project document into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertProposalVote(int $userId, int $proposalId, bool $vote): int {
		return parent::insert([
			self::COLUMN_USER_ID => $userId,
			self::COLUMN_PROPOSAL_ID => $proposalId,
			self::COLUMN_VOTE => $vote
		]);
	}

	/**
	 *
	 * @param $proposalVote
	 */
	protected function convertEntityToArray($proposalVote): array {
		throw new NotImplementedError('ne');
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): ProposalVoteRepository {
		$user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		$proposal = $this->proposalRepository->getById($row[self::COLUMN_PROPOSAL_ID]);
		return new Document(
            new EntityMetadata($row),
			$user,
			$proposal,
			$row[self::COLUMN_VOTE]
		);
	}
}