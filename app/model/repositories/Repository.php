<?php
namespace App\Model;

use App\Exception\NotImplementedError;
use Nette;
use Nette\Utils\DateTime;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\Table\Selection;




/**
 * Abstract class. All Repositories have to extends from it.
 */
abstract class Repository {
	use Nette\SmartObject;

	/** @var Context - reference to database */
	protected $database;

	/** @var array - cached entities */
	protected $entities = [];

	/** @var string table name - defined in child classes */
	const TABLE_NAME = '';

	/** General column names */
	const
		COLUMN_ID = 'id',
		COLUMN_CREATED_AT = 'created_at',
		COLUMN_UPDATED_AT = 'updated_at',
		COLUMN_DELETED = 'deleted';


	public function __construct(Context $database)
    {
		$this->database = $database;
	}




    /**
     * Factory method which creates an Entity based on an IRow.
     * Return type is specified only in inheriting classes because each has a different one.
     *
     * @param $row
     * @return Entity
     */
    protected abstract function convertRowToEntity(IRow $row);
    /**
     * Convert an Entity to an array of key-value pairs where the key is a column name
     * in the DB table and value is it's value.
     * Param type is specified only in inheriting classes because each has a different one.
     *
     * @param $entity
     * @return array of values for update command
     */
    protected abstract function convertEntityToArray($entity): array;










    /**
	 * @return Entity object or null if not found
	 */
	public function getById(int $id): ?Entity
    {
		return $this->getByColumn(self::COLUMN_ID, $id);
	}
	/**
	 * Get all records from database as Entity objects
	 *
	 * @return Entity[]
	 */
	public function getAll(): array
    {
		$table = $this->table();
		$result = array();
		foreach ($table as $row) {
			if ($row[self::COLUMN_DELETED] == 0) {
				$record = $this->rowToEntity($row);
				array_push($result, $record);
			}
		}
		return $result;
	}
	/**
	 * @param Entity $entity
	 * @return array names of changed columns (always includes 'updated_at')
	 */
	public function update(Entity $entity): array
    {
		$id = $entity->getId();
        $keyValuePairs = $this->convertEntityToArray($entity);
        $keyValuePairs[self::COLUMN_UPDATED_AT] = new DateTime();
        $row = $this->getRow(self::COLUMN_ID, $id);
        $this->table()
            ->where(self::COLUMN_ID, $id)
            ->update($keyValuePairs);
		$changedColumns = [];
		foreach ($keyValuePairs as $key => $value){
			if ($value != $row[$key]){
                $changedColumns[] = $key;
			}
		}
		return $changedColumns;
	}
	public function deleteById(int $id): void
    {
		$this->table()
			->where(self::COLUMN_ID, $id)
			->update([
				self::COLUMN_UPDATED_AT => new DateTime(),
				self::COLUMN_DELETED => 1
			]);
		unset($this->entities[$id]);


        /*
        $this->table()
        ->where(self::COLUMN_ID, $id)
        ->delete();
        */
	}
	public function deleteEntity(Entity $entity): void
    {
        $this->deleteById($entity->getId());
	}









	protected function table(): Selection
    {
		return $this->database->table(static::TABLE_NAME);
	}
	/**
	 * @param array $values array of key-value pairs describing the field of an entity
	 * @return int database ID of the newly inserted row
	 */
	protected function insert(array $values): int
    {
		$now = new DateTime();
		$values[self::COLUMN_CREATED_AT] = $now;
		$values[self::COLUMN_UPDATED_AT] = $now;
		$values[self::COLUMN_DELETED] = 0;
		$row = $this->table()->insert($values);
		$id = $row[self::COLUMN_ID];
		return $id;
	}
	/**
	 * Return all values from particular column in array
	 *
	 * @return array of Database values from particular column
	 */
	protected function getAllFromColumn(string $columnName): array
    {
		$result = [];
		$rows = $this->table();
		foreach ($rows as $row) {
			$result[] = $row[$columnName];
		}
		$result = array_unique($result);
		return $result;
	}
	/**
	 * @param $value
	 * @param $columnName
	 * @return Entity having the specified value in the specified column. Null if not found.
	 */
	protected function getByColumn(string $columnName, $value = null): ?Entity
    {
		if ($columnName == self::COLUMN_ID && array_key_exists($value, $this->entities)) {
			return $this->entities[$value];
		}
		$row = $this->getRow($columnName, $value);
		return $row ? $this->rowToEntity($row) : null;
	}
	/**
	 * Return array, where value from the column
	 * represent key and value is an array of all values
	 * associated with that value
	 *
	 * @param $columnName
	 */
	protected function getGroupBy(string $columnName)
    {
	    throw new NotImplementedError('get group by');
	}
	/**
	 * Get entity of the row. Return the same object instance
	 * for the entity with the same type and same ID.
	 *
	 * @param  IRow $row
	 * @return Entity
	 */
	protected function rowToEntity(IRow $row): Entity
    {
		$id = $row[self::COLUMN_ID];
		if (!array_key_exists($id, $this->entities)) {
			$this->entities[$id] = $this->convertRowToEntity($row);
		}
		return $this->entities[$id];
	}





    /**
     * @param string $columnName
     * @param $value
     * @return IRow|null the first row that satisfies the criteria or null
     */
	private function getRow(string $columnName, $value): ?IRow
    {
		$row = $this->table()
			->where($columnName, $value)
			->where(self::COLUMN_DELETED, 0)
			->fetch();
		return $row ? $row : null;
	}
}