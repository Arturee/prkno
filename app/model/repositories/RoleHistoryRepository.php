<?php
namespace App\Model;

use Nette;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Database\Context;
use Nette\Database\IRow;

/**
 * Represents class managing documents in database
 */
class RoleHistoryRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'role_history',
		COLUMN_USER_ID = 'user_id',
		COLUMN_ROLE = 'role',
		COLUMN_COMMENT = 'comment',
		COLUMN_VALID_SINCE = 'valid_since';

	/** @var UserRepository */
	private $userRepository;

	/**
	 * Create DocumentRepository
	 * 
	 * @param $database     Database dependency injection
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
	}

	/**
	 * Insert new project document into database
	 *
	 * @return ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertRoleHistory(int $userId, string $role, string $comment, DateTime $validSince): int {
		return parent::insert([
			self::COLUMN_USER_ID => $userId,
			self::COLUMN_ROLE => $role,
			self::COLUMN_COMMENT => $comment,
			self::COLUMN_VALID_SINCE => $validSince
		]);
	}

	/**
	 *
	 * @param $roleHistory
	 */
	protected function convertEntityToArray($roleHistory): array {
		return [];
	}

	/**
	 * Create Document object from database row
	 * 
	 * @param $row  Database row
	 * @return Document
	 */
	protected function convertRowToEntity(IRow $row): Document {
		$user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		return new RoleHistory(
		    new EntityMetadata($row),
			$user,
			$row[self::COLUMN_ROLE],
			$row[self::COLUMN_COMMENT],
			$row[self::COLUMN_VALID_SINCE]
		);
	}
}