<?php
namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\UniqueConstraintViolationException;

/**
 * Manager for data in user_on_project table
 */
class UserOnProjectRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'users_on_project',
		COLUMN_USER_ID = 'user_id',
		COLUMN_PROJECT_ID = 'project_id',
		COLUMN_PROJECT_ROLE = 'project_role',
		COLUMN_COMMENT = 'comment';

	/** @var UserRepository */
	private $userRepository;

	/** @var ProjectRepository */
	private $projectRepository;

	/**
	 * UserOnProjectRepository constructor.
	 *
	 * @param $database         Database
	 * @param $userRepository   User repository
	 */
	public function __construct(
		Context $database,
		UserRepository $userRepository,
		ProjectRepository $projectRepository
	) {
		parent::__construct($database);
		$this->userRepository = $userRepository;
		$this->projectRepository = $projectRepository;
	}

	/**
	 * Insett user on project
	 *
	 * @param $userId       User ID 
	 * @param $projectId    Project ID
	 * @param $projectRole  Project role
	 * @return int          ID of the inserted row
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	public function insertUserOnProject(int $userId, int $projectId, string $projectRole): int {
		return parent::insert([
			self::COLUMN_USER_ID => $userId,
			self::COLUMN_PROJECT_ID => $projectId,
			self::COLUMN_PROJECT_ROLE => $projectRole
		]);
	}

	/**
	 * Get users on particular project with specific ID
	 * User data has type UserOnProject.
	 *
	 * @param $projectId Project ID
	 * @param $projectRole 	For filtering only users with specific role, if not defined, get all users
	 * @return array<UserOnProject>     Array of UserOnProject data (empty if no user found)
	 */
	public function getUsersOnProject(int $projectId, string $projectRole = null): array {
		$rows = $this->table()
			->where(self::COLUMN_PROJECT_ID, $projectId);
		$usersOnProject = [];
		foreach ($rows as $row) {
			$userOnProject = $this->getByRow($row);
			$role = $userOnProject->getProjectRole();
			if ($projectRole == null || $role == $projectRole) {
				array_push($usersOnProject, $userOnProject);
			}
		}
		return $usersOnProject;
	}

	/**
	 * @param $project 		Project ID
	 * @param $projectRole 	Project role
	 */
	public function getUserOnProject(int $projectId, string $projectRole = null): ?UserOnProject {
		$members = $this->getUsersOnProject($projectId, $projectRole);
		return sizeof($members) > 0 ? $members[0] : null;
	}

	/**
	 * Get all supervisors on project
	 *
	 * @param $project 	Project ID
	 * @return User of users with supervisor role on project
	 */
	public function getSupervisor(int $projectId): ?UserOnProject {
		return $this->getUserOnProject($projectId, UserOnProject::PROJECT_ROLE_SUPERVISOR);
	}

	/**
	 * Get list of Users which have a role as consultant in project
	 *
	 * @param $project 	Project ID
	 * @return Array of users with consultant role on project
	 */
	public function getConsultants(int $projectId): array {
		return $this->getUsersOnProject($projectId, UserOnProject::PROJECT_ROLE_CONSULTANT);
	}

	/**
	 * Get all students on project
	 *
	 * @param $project 	Project ID
	 * @return Array of users with student role on project
	 */
	public function getStudents(int $projectId): array {
		return $this->getUsersOnProject($projectId, UserOnProject::PROJECT_ROLE_TEAM_MEMBER);
	}

	/**
	 * Get opoonent, or null if not assigned
	 *
	 * @param $project 	Project ID
	 * @return User which is an opponent for given project, or null, it not assigned yet
	 */
	public function getOpponent(int $projectId): ?UserOnProject {
		return $this->getUserOnProject($projectId, UserOnProject::PROJECT_ROLE_OPPONENT);
	}

	/**
	 *
	 * @param $userOnProject
	 */
	protected function convertEntityToArray($userOnProject): array {
		return [
			self::COLUMN_PROJECT_ROLE => $userOnProject->getProjectRole(),
			self::COLUMN_COMMENT => $userOnProject->getComment()
		];
	}

	/**
	 * Create UserOnProject object from database row data
	 *
	 * @param $row  			User on project from database as table row
	 * @return UserOnProject  	User on project entity
	 */
	protected function convertRowToEntity(IRow $row) {
		$user = $this->userRepository->getById($row[self::COLUMN_USER_ID]);
		$project = $this->projectRepository->getById($row[self::COLUMN_PROJECT_ID]);
		return new UserOnProject(
            new EntityMetadata($row),
			$user,
			$project,
			$row[self::COLUMN_PROJECT_ROLE],
			$row[self::COLUMN_COMMENT]
		);
	}
}