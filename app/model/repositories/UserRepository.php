<?php
namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\Database\IRow;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Security\Passwords;
use Nette\Utils\DateTime;
use App\Lang\Lang;

/**
 *
 * CARE Nette bug: mysql/Nette instead of throwing and exception when impossible
 * value is given to enum it just puts and empty string (NOT even null)!
 * And even though empty string is not allowed in the enum!
 *
 *
 * Represents class managing users' data in the database
 */
class UserRepository extends Repository {
	use Nette\SmartObject;

	/** Column names */
	const
		TABLE_NAME = 'users',

        //for a normal user, who uses CAS:
		COLUMN_NAME = 'name',
        COLUMN_EMAIL = 'email',
        COLUMN_LAST_LOGIN = 'last_login',
        COLUMN_LOGIN_OPTION = 'login_option',
        COLUMN_BANNED = 'banned',
        COLUMN_ROLE = 'role',
        COLUMN_LAST_CAS_REFRESH = 'last_CAS_refresh',
        COLUMN_ROLE_IS_FROM_CAS = 'role_is_from_CAS',

        //stuff that anybody can edit about them selves:
        COLUMN_DEGREE_PREFIX = 'degree_prefix',
        COLUMN_DEGREE_SUFFIX = 'degree_sufix',
        COLUMN_NEWS_SUBSCRIPTION_LANG = 'news_subscription_lang',
        COLUMN_CUSTOM_URL = 'custom_url',

        //for an abnormal manual user:
		COLUMN_PASSWORD_HASH = 'password',
        COLUMN_ROLE_EXPIRATION = 'role_expiration',
        COLUMN_PASSWORD_RECOVERY_TOKEN = 'password_recovery_token',
        COLUMN_PASSWORD_RECOVERY_TOKEN_EXPIRATION = 'password_recovery_token_expiration',
        COLUMN_MANUALLY_CREATED = 'manually_created_at';



    public function __construct(Context $database)
    {
		parent::__construct($database);
	}

	public function insertUser(
		string $name,
		string $password,
		string $email,
		string $role,
        ?string $degreePrefix,
        ?string $degreeSufix
	) : int
    {
		$passwordHash = Passwords::hash($password);
        if ($role === User::ROLE_GUEST){
            $role = null;
        }
		$id = $this->insert([
            self::COLUMN_DEGREE_PREFIX => $degreePrefix,
            self::COLUMN_NAME => $name,
            self::COLUMN_DEGREE_SUFFIX => $degreeSufix,
            self::COLUMN_EMAIL => $email,
            self::COLUMN_PASSWORD_HASH => $passwordHash,
            self::COLUMN_MANUALLY_CREATED => true,
            self::COLUMN_LOGIN_OPTION => User::LOGIN_OPTION_DB,
            self::COLUMN_ROLE => $role,
            self::COLUMN_LAST_LOGIN => null,
            self::COLUMN_BANNED => false,
            self::COLUMN_NEWS_SUBSCRIPTION_LANG => Lang::CS
        ]);
		return $id;
	}

	public function insertUserFromCas(CasInfo $casInfo) : int
    {
        $role = $casInfo->getRole();
        if ($role === User::ROLE_GUEST){
            $role = null;
        }
		$id = $this->insert([
			self::COLUMN_NAME => $casInfo->getFullName(),
			self::COLUMN_EMAIL => $casInfo->getEmail(),
			self::COLUMN_MANUALLY_CREATED => false,
			self::COLUMN_LOGIN_OPTION => User::LOGIN_OPTION_CAS,
			self::COLUMN_ROLE => $role,
            self::COLUMN_LAST_CAS_REFRESH => new DateTime(),
            self::COLUMN_ROLE_IS_FROM_CAS => true,
            self::COLUMN_BANNED => false,
            self::COLUMN_NEWS_SUBSCRIPTION_LANG => Lang::CS
		]);
        return $id;
	}

	public function getUserByEmail(string $email): ?User
    {
        $user = $this->getByColumn(self::COLUMN_EMAIL, $email);
		return $user;
	}


	/**
	 * Update values of given User
	 *
	 * @param $user    User data
	 * @param $values   Array of values key => value    where key represents column name
	 * @throws UniqueConstraintViolationException    If duplicate value with unique key
	 */
	//TODO will be removed and function update(User) will be used instead
	public function updateValues(User $user, array $values)
	{
		if (isset($values["password"])) {
			if (empty($values["password"])) {
				unset($values["password"]);
			} else {
				$values["password"] = Passwords::hash($values["password"]);
			}
		}
		try {
			$userId = $user->getID();
			$this->table()
				->where(self::COLUMN_ID, $userId)
				->update($values);
		} catch (UniqueConstraintViolationException $e) {
			throw $e;
		}
	}

	/**
	 *
	 * @param $user
	 */
	protected function convertEntityToArray($user): array
	{
	    /** @var User $user */
		$role = $user->getRole();
        if (!$role == User::ROLE_GUEST) {
            $role = null;
        }
		return [
			self::COLUMN_NAME => $user->getName(),
        	self::COLUMN_EMAIL => $user->getEmail(),
        	self::COLUMN_LAST_LOGIN => $user->getLastLogin(),
        	self::COLUMN_LOGIN_OPTION => $user->getLoginOption(),
        	self::COLUMN_BANNED => $user->isBanned(),
        	self::COLUMN_ROLE => $role,
        	self::COLUMN_LAST_CAS_REFRESH => $user->getLastCasRefresh(),
        	self::COLUMN_ROLE_IS_FROM_CAS => $user->isRoleFromCas(),
        	self::COLUMN_DEGREE_PREFIX => $user->getDegreePrefix(),
        	self::COLUMN_DEGREE_SUFFIX => $user->getDegreeSuffix(),
        	self::COLUMN_NEWS_SUBSCRIPTION_LANG => $user->getNewsSubscriptionLanguage(),
        	self::COLUMN_CUSTOM_URL => $user->getCustomUrl(),
			self::COLUMN_PASSWORD_HASH => $user->getPasswordHash(),
        	self::COLUMN_ROLE_EXPIRATION => $user->getRoleExpiration(),
        	self::COLUMN_PASSWORD_RECOVERY_TOKEN => $user->getPasswordRecoveryToken(),
        	self::COLUMN_PASSWORD_RECOVERY_TOKEN_EXPIRATION => $user->getPasswordRecoveryTokenExpiration(),
        	self::COLUMN_MANUALLY_CREATED => $user->isCreatedManually()
		];
	}

    /**
     * @Override
     */
	protected function convertRowToEntity(IRow $row): User
	{
        $role = $row[self::COLUMN_ROLE];
        if (!$role) {
            $role = User::ROLE_GUEST;
        }
		$user = new User(
		    new EntityMetadata($row),
			$row[self::COLUMN_NAME],
            $row[self::COLUMN_EMAIL],
            $role,
            $row[self::COLUMN_MANUALLY_CREATED],
            $row[self::COLUMN_BANNED],
            $row[self::COLUMN_LOGIN_OPTION],
            $row[self::COLUMN_ROLE_IS_FROM_CAS],
            $row[self::COLUMN_PASSWORD_HASH],
            $row[self::COLUMN_NEWS_SUBSCRIPTION_LANG],
            $row[self::COLUMN_LAST_LOGIN],
            $row[self::COLUMN_ROLE_EXPIRATION],
			$row[self::COLUMN_DEGREE_PREFIX],
			$row[self::COLUMN_DEGREE_SUFFIX],
            $row[self::COLUMN_CUSTOM_URL],
            $row[self::COLUMN_LAST_CAS_REFRESH],
            $row[self::COLUMN_PASSWORD_RECOVERY_TOKEN],
            $row[self::COLUMN_PASSWORD_RECOVERY_TOKEN_EXPIRATION]
		);
		return $user;
	}
}