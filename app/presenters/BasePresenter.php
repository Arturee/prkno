<?php
namespace App\Presenters;

use App\Controls\SigninControl;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;
use App\Controls\SigninControlFactory;
use App\Model\Transaction;
use App\Utils\Logger;
use App\Model\User;
use App\Model\UserRepository;
use App\Model\Acl;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter {

	/** @persistent */
	public $locale; //used by kdyby translation to distinguish language


	/** @var Translator @inject */
	public $translator;
	
	/** @var SigninControlFactory @inject */
	public $signinControlFactory;

	/** @var Transaction @inject */
	public $transaction;

	/** @var Logger @inject */
	public $logger;

	/** @var UserRepository @inject */
	public $userRepository;


	/**
     * @Override
	 * Run initial code
	 */
	public function startup()
    {
        parent::startup();
		$this->transaction->beginTransaction();
	}

	/**
     * @Override
	 * @param $response 	TODO type???
	 */
	public function shutdown($response)
    {
		$this->transaction->commit();
        parent::shutdown($response);
	}

	/**
     * @Override
	 * Method running before template rendering
	 */
	public function beforeRender()
    {
		parent::beforeRender();



		$menuItems = [
		    [
		        'name' => 'home',
                'link' => 'Homepage:',
                'resource' => Acl::RESOURCE_DASHBOARD
            ],
            [
                'name' => 'projects',
                'link' => 'Projects:',
                'resource' => Acl::RESOURCE_PROJECTS
            ],
            [
                'name' => 'advertisements',
                'link' => 'Ads:',
                'resource' => Acl::RESOURCE_ADS
            ],
            [
                'name' => 'users',
                'link' => 'Users:',
                'resource' => Acl::RESOURCE_USERS
            ],
            [
                'name' => 'documents',
                'link' => 'StaticPages:',
                'resource' => Acl::RESOURCE_STATIC_PAGES
            ],
            [
                'name' => 'err',
                'link' => 'ErrorCodes:',
                'resource' => Acl::RESOURCE_ERROR_CODES
            ]
        ];
		$user = $this->getUser();
		$this->template->menuItems = array_filter($menuItems, function($item) use ($user){
		    return $user->isAllowed($item['resource'], Acl::ACTION_VIEW);
        });



        if ($this->getUser()->isLoggedIn()){
            /**
             * Uncomment if you ran ./up.sh and forgot to log out.
             */
            //$this->getUser()->logout(); return;

			
            $userId = $this->getUser()->getId();
            /** @var User $user */
            $user = $this->userRepository->getById($userId);
            $this->template->userName = $user->getName();
            $this->template->usesCAS = $user->isLoggedInViaCas();
        }
		$this->template->isLocaleCs = ($this->locale === 'cs');
	}

	public function createComponentSigninControl() : SigninControl
    {
		$presenter = $this;
		$signinControl = $this->signinControlFactory->create();
		$signinControl->onLogin[] = function($control) use ($presenter) {
			$presenter->redirectToDefaultView(['page.layout.message.signInSuccess']);
		};
		$signinControl->onLogout[] = function($control) use ($presenter) {
			$presenter->redirectToDefaultView(['page.layout.message.signOutSuccess']);
		};
		return $signinControl;
	}













	private function redirectToDefaultView($flashesToTranslate) : void
    {
		foreach ($flashesToTranslate as $flash){
			$this->flashMessage($this->translator->trans($flash));
		}
		$this->redirect(':');
	}
}
