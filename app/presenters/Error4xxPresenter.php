<?php

namespace App\Presenters;

use Nette;
use Nette\Utils\Strings;


class Error4xxPresenter extends BasePresenter
{

    public function startup()
	{
		parent::startup();
		if (!$this->getRequest()->isMethod(Nette\Application\Request::FORWARD)) {
			$this->error();
		}
	}


	public function renderDefault(Nette\Application\BadRequestException $exception)
	{
        $this->fixLocale();
		// load template 403.latte or 404.latte or ... 4xx.latte
		$file = __DIR__ . "/templates/Error/{$exception->getCode()}.latte";
		$this->template->setFile(is_file($file) ? $file : __DIR__ . '/templates/Error/4xx.latte');
	}






	private function fixLocale()
    {
        /**
         * CARE workaround
         * This is a workaround since somehow $this->locale is not set here (is null)
         * and therefore translator->locale is not set either (is null).
         * That is probably the desired behaviour of Nette tho (not a bug in Nette).
         *
         * Not completely sure why both are null.
         * It is impossible to inject Http request here, it results in a 500 error.
         *
         */
        $this->locale = 'cs';
        if (Strings::contains($_SERVER['REQUEST_URI'], '/en/')){
            $this->locale = 'en';
        }
        $this->translator->setLocale($this->locale);
    }

}
