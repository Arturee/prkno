<?php

namespace App\Presenters;

use Nette;
use Nette\Application\Responses;
use Tracy\ILogger;


class ErrorPresenter implements Nette\Application\IPresenter
{
	use Nette\SmartObject;

	/** @var ILogger */
	private $logger;


	public function __construct(ILogger $logger)
	{
		$this->logger = $logger;
	}


	/**
	 * @return Nette\Application\IResponse
	 */
	public function run(Nette\Application\Request $request)
	{
		$e = $request->getParameter('exception');

		if ($e instanceof Nette\Application\BadRequestException) {
			// $this->logger->log("HTTP code {$e->getCode()}: {$e->getMessage()} in {$e->getFile()}:{$e->getLine()}", 'access');
			list($module, , $sep) = Nette\Application\Helpers::splitName($request->getPresenterName());
			return new Responses\ForwardResponse($request->setPresenterName($module . $sep . 'Error4xx'));
		}

		$this->logger->log($e, ILogger::EXCEPTION);


        $locale = $this->getLocale($request);
		return new Responses\CallbackResponse(function () use ($locale) {
            if ($locale === 'cs') {
                $errorTemplate = '500_cs.phtml';
            } else {
                $errorTemplate = '500.phtml';
            }
			require __DIR__ . '/templates/Error/' . $errorTemplate;
		});
	}




    private function getLocale(Nette\Application\Request $request) : string
    {
        /**
         * CARE
         * It is impossible to use $_SERVER[] here, it results in an exception.
         */

        /** @var Nette\Application\Request $applicationRequest */
        $applicationRequest = $request->getParameters()['request'];
        $locale = $applicationRequest->getParameter('locale');
        return $locale;
    }
}
