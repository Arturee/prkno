<?php
namespace App\Presenters;

use App\Forms\AddNewsFormFactory;
use App\Forms\FormFactory;
use App\Model\News;
use App\Model\NewsRepository;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use App\Model\CasClient;
use App\Model\Acl;
use Nette\Security\AuthenticationException;
use Nette\Utils\DateTime;

/**
 * Dashboard is here (on homepage)
 *
 * //TODO use i18n
 *
 * //TODO logging
 * //TODO exception handling
 *
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends BasePresenter {
    /** @var NewsRepository */
    private $newsRepository;
    /** @var CasClient */
    private $casClient;
    /** @var AddNewsFormFactory */
    private $addNewsFormFactory;

    public function __construct(
        NewsRepository $newsRepository,
        CasClient $casClient,
        AddNewsFormFactory $addNewsFormFactory
    ) {
        parent::__construct();
        $this->newsRepository = $newsRepository;
        $this->casClient = $casClient;
        $this->addNewsFormFactory = $addNewsFormFactory;
    }


    /**
     * @Override
     */
    public function startup() : void
    {
        parent::startup();
        $wasCasAuthenticationSuccessful = $this->casClient->isCurrentUserAuthenticated() && !$this->user->isLoggedIn();
        if ($wasCasAuthenticationSuccessful){
            /**
             * the user just logged in via CAS, and then CAS redirected here
             */
            try {
                $this->getUser()->login();
            } catch(AuthenticationException $e){
                $this->flashMessage($e->getMessage());
            }
        }
    }

    public function renderDefault() : void
    {
        $news = [];
        /** @var News $newsItem */
        foreach ($this->newsRepository->getAll() as $newsItem){
            $title = $newsItem->getTitle($this->template->isLocaleCs);
            $content = $newsItem->getContent($this->template->isLocaleCs);
            $class = $newsItem->isImportant() ? 'panel-danger' : 'panel-info';
            $news[] = [
                'private' => !$newsItem->isVisibleToGuests(),
                'class' => $class,
                'title' => $title,
                'content' => $content,
                'updated' => $newsItem->updatedAt()
            ];
        }
        if (!$this->getUser()->isAllowed(Acl::RESOURCE_NEWS, Acl::ACTION_VIEW_PRIVATE)){
            $news = array_filter($news, function($item){return $item['private'] !== true;});
        }
        usort($news, function($a, $b){
            /**
             * @var DateTime $aTime
             * @var DateTime $bTime
             */
            $aTime = $a['updated'];
            $bTime = $b['updated'];
            $minuteDifference = $aTime->diff($bTime)->i;
            return $minuteDifference;
        });
        $this->template->news = $news;
        $this->template->canNewsCreate = $this->getUser()->isAllowed(Acl::RESOURCE_NEWS, Acl::ACTION_CREATE);
    }
    public function actionCasLoginRedirect() : void
    {
        if ($this->casClient->isCurrentUserAuthenticated() || $this->getUser()->isLoggedIn()){
            /**
             * 99% probability - The cuni.cz CAS website has redirected here after a successful authentication.
             * 1% probability - a bug or a hacker is trying to access this action as a view. Which is not a problem
             * at all since we just redirect and do nothing else.
             */
            $this->redirect(\Nette\Http\IResponse::S302_FOUND, 'Homepage:default');
            //LATER maybe try to avoid this "redundant" redirect
        }
        $this->casClient->redirectToCasForAuthentication();
    }
    public function actionCasLogoutRedirect() : void
    {
        if (!$this->casClient->isCurrentUserAuthenticated() || !$this->getUser()->isLoggedIn()){
            //TODO log bug hack
            return;
        }

        $this->getUser()->logout();
        $absoluteUrlToRedirectTo = $this->link('//Homepage:default');
        $this->casClient->logout($absoluteUrlToRedirectTo);
    }






    protected function createComponentAddNews() : Form
    {
        $presenter = $this;
        $form = $this->addNewsFormFactory->create();
        $form->onSuccess[] = function() use ($presenter) {
            $presenter->flashMessage($this->translator->translate('control.news.add.message.success'));
            $presenter->redirect('Homepage:');
        };
        return $form;
    }

}
