<?php
namespace App\Presenters;

use Nette;
use App\Model\ProjectRepository;
use App\Model\UserOnProjectRepository;
use Nette\Application\UI\Form;

/**
 * Class ProjectPresenter
 * @package App\Presenters
 */
class ProjectPresenter extends BasePresenter {

    /** @var  ProjectRepository */
    private $projectRepository;

    /** @var UserOnProjectRepository */
    private $userOnProjectRepository;

    /**
     *
     * @param $projectRepository
     * @param $userOnProjectRepository
     */
    public function __construct(
        ProjectRepository $projectRepository,
        UserOnProjectRepository $userOnProjectRepository
    ) {
        $this->projectRepository = $projectRepository;
        $this->userOnProjectRepository = $userOnProjectRepository;
    }

    /**
     *
     * @param $id
     */
    public function renderShow(int $id) {
        $this->template->project = $this->projectRepository->getById($id);
        
        $this->template->supervisor = $this->userOnProjectRepository->getSupervisor($id);
        $this->template->opponent = $this->userOnProjectRepository->getOpponent($id);

        $this->template->consultants = $this->userOnProjectRepository->getConsultants($id);
        $this->template->students = $this->userOnProjectRepository->getStudents($id);
    }

}