<?php

namespace App\Presenters;

use App\Model\UserOnProjectRepository;
use Nette;
use App\Forms\NewProjectFormFactory;
use App\Model\ProjectRepository;
use Nette\Application\UI\Form;

/**
 * Class ProjectsPresenter
 * @package App\Presenters
 */
class ProjectsPresenter extends BasePresenter {

    /** @var  ProjectRepository */
    private $projectRepository;

    /** @var NewProjectFormFactory */
    private $newProjectFactory;

    /** @var UserOnProjectRepository */
    private $userOnProjectRepository;

    /**
     *
     *
     * @param $projectRepository
     * @param $newProjectFactory
     * @param $userOnProjectRepository
     */
    public function __construct(
        ProjectRepository $projectRepository,
        NewProjectFormFactory $newProjectFactory,
        UserOnProjectRepository $userOnProjectRepository
    ) {
        $this->projectRepository = $projectRepository;
        $this->newProjectFactory = $newProjectFactory;
        $this->userOnProjectRepository = $userOnProjectRepository;
    }

    public function renderDefault()
    {
        /*include("../bin/test-data.php") ;  create users for test */
        $projects = $this->projectRepository->getAll();

        //TODO get supervisors

        $this->template->projects = $projects;
    }

    /**
     * New Project form factory.
     * @return Form
     */
    protected function createComponentNewProjectForm()
    {

        $form = $this->newProjectFactory->create(function() {
            //$this->flashMessage($this->translator->translate('page.user.profile.message.updated'));
        });

        $form['supervisor']->value = $this->user->identity->user->getName();
        $form['supervisor_id']->value = $this->user->identity->user->getId();

        $form->onSuccess[] = function ($form) {
            $this->flashMessage($this->translator->translate('control.newProject.message.success'));
        };
        return $form;
    }

}