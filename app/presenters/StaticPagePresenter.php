<?php
namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\PageVersionRepository;

class StaticPagePresenter extends BasePresenter {
    /** @var PageVersionRepository */
    private $pageVersionRepository;

	/**
     *
     * @param $pageVersionRepository
     */
	public function __construct(
        PageVersionRepository $pageVersionRepository
    ) {
        $this->pageVersionRepository = $pageVersionRepository;
    }

    /**
     * @param $id
     */
    public function renderDefault(int $id = null) {
    	$pageVersion = $this->pageVersionRepository->getById($id);
    	$this->template->page = $pageVersion;
    }
}