<?php
namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\PageVersionRepository;

class StaticPagesPresenter extends BasePresenter {
    /** @var PageVersionRepository */
    private $pageVersionRepository;

    /**
     *
     * @param $pageVersionRepository
     */
	public function __construct(
        PageVersionRepository $pageVersionRepository
    ) {
        $this->pageVersionRepository = $pageVersionRepository;
    }

    public function renderDefault() {
        $this->template->pages = $this->pageVersionRepository->getAll();
    }
}