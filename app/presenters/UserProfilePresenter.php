<?php

namespace App\Presenters;

use Nette;
use App\Forms;


class UserProfilePresenter extends BasePresenter
{

	/** @var Forms\UserProfileFormFactory */
	private $userProfileFactory;

	public function __construct(Forms\UserProfileFormFactory $userProfileFactory)
	{
		$this->userProfileFactory = $userProfileFactory;
	}

	protected function createComponentUserProfileForm()
	{
		return $this->userProfileFactory->createEdit(function () {
			$this->flashMessage($this->translator->translate('_page.user.profile.message.updated'));
		});
	}

}
