-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Čas generovania: Ne 17.Sep 2017, 18:44
-- Verzia serveru: 5.5.57-0ubuntu0.14.04.1
-- Verzia PHP: 7.1.9-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `prkno`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` int(11) DEFAULT NULL,
  `type` enum('project','person') DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `subscribed_via_email` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `ad_comments`
--

CREATE TABLE `ad_comments` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `defence_attendance`
--

CREATE TABLE `defence_attendance` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vote` enum('yes','no') DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `must_come` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `defence_dates`
--

CREATE TABLE `defence_dates` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `date` date DEFAULT NULL,
  `signup_deadline` date DEFAULT NULL,
  `active_voting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content_md` text,
  `fallback_path` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `keywords`
--

CREATE TABLE `keywords` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `word` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `document_id_cs` int(11) DEFAULT NULL,
  `document_id_en` int(11) DEFAULT NULL,
  `importance` tinyint(1) NOT NULL DEFAULT '0',
  `guest_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `page_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `page_versions`
--

CREATE TABLE `page_versions` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `document_id_cs` int(11) NOT NULL,
  `document_id_en` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed','canceled') NOT NULL,
  `run_start_date` datetime DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `short_name` varchar(45) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `content` int(11) DEFAULT NULL,
  `waiting_for_proposal_evaluation` tinyint(1) NOT NULL DEFAULT 1,
  `requested_to_run` tinyint(1) NOT NULL DEFAULT 0,
  `extra_credits` int(11) DEFAULT NULL,
  `team_members_visible` tinyint(1) NOT NULL DEFAULT 0,
  `keywords` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `project_defence`
--

CREATE TABLE `project_defence` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `defence_date_id` int(11) DEFAULT NULL,
  `opponent_id` int(11) DEFAULT NULL,
  `statement_author_id` int(11) DEFAULT NULL,
  `room` varchar(200) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `defence_statement` int(11) DEFAULT NULL,
  `opponent_review` int(11) DEFAULT NULL,
  `supervisor_review` int(11) DEFAULT NULL,
  `defended` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `project_document`
--

CREATE TABLE `project_document` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `team_private` tinyint(1) NOT NULL,
  `date_submitted` date DEFAULT NULL,
  `defence_number` int(11) DEFAULT NULL,
  `type` enum('for_defense','read_only','advice') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `project_state_history`
--

CREATE TABLE `project_state_history` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `state` enum('proposal','accepted','analysis','analysis_handed_in','implementation','implementation_handed_in','conditionally_defended','conditional_documents_handed_in','defended','failed') NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `valid_since` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `proposals`
--

CREATE TABLE `proposals` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `state` enum('draft','sent','accepted','declined') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `proposal_comments`
--

CREATE TABLE `proposal_comments` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `committee_only` tinyint(1) NOT NULL,
  `send_email` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `proposal_votes`
--

CREATE TABLE `proposal_votes` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `vote` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `role_history`
--

CREATE TABLE `role_history` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` enum('team_member','supervisor','opponent','consultant') DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `valid_since` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `degree_prefix` varchar(45) DEFAULT NULL,
  `degree_sufix` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varbinary(200) DEFAULT NULL,
  `news_subscription_lang` enum('cs','en') NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `manually_created_at` tinyint(1) NOT NULL,
  `custom_url` varchar(500) DEFAULT NULL,
  `last_CAS_refresh` datetime DEFAULT NULL,
  `role` enum('student','academic','committee_member','committee_chair','committee_secretary') DEFAULT NULL,
  `role_expiration` datetime DEFAULT NULL,
  `role_is_from_CAS` tinyint(1) NOT NULL,
  `login_option` enum('cas','db') NOT NULL,
  `password_recovery_token` varchar(100) DEFAULT NULL,
  `password_recovery_token_expiration` datetime DEFAULT NULL,
  `banned` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users_on_project`
--

CREATE TABLE `users_on_project` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_role` enum('team_member','supervisor','opponent','consultant') NOT NULL,
  `comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertisements_user_id_idx` (`user_id`),
  ADD KEY `advertisements_content_idx` (`content`);

--
-- Indexy pre tabuľku `ad_comments`
--
ALTER TABLE `ad_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ad_comments_ad_id_idx` (`ad_id`),
  ADD KEY `ad_comments_user_id_idx` (`user_id`);

--
-- Indexy pre tabuľku `defence_attendance`
--
ALTER TABLE `defence_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `defence_attendance_user_id_idx` (`user_id`);

--
-- Indexy pre tabuľku `defence_dates`
--
ALTER TABLE `defence_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_user_id_idx` (`user_id`),
  ADD KEY `news_document_id_en_idx` (`document_id_en`),
  ADD KEY `news_document_id_cs_idx` (`document_id_cs`);

--
-- Indexy pre tabuľku `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `page_versions`
--
ALTER TABLE `page_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_page_id_idx` (`page_id`),
  ADD KEY `pages_user_id_idx` (`user_id`),
  ADD KEY `pages_document_id_cs_idx` (`document_id_cs`),
  ADD KEY `pages_document_id_en_idx` (`document_id_en`);

--
-- Indexy pre tabuľku `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `projects_short_name_uq` (`short_name`),
  ADD KEY `projects_content_idx` (`content`);

--
-- Indexy pre tabuľku `project_defence`
--
ALTER TABLE `project_defence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_defence_project_id_idx` (`project_id`),
  ADD KEY `project_defence_defence_date_id_idx` (`defence_date_id`),
  ADD KEY `project_defence_statement_author_id_idx` (`statement_author_id`),
  ADD KEY `project_defence_defence_statement_idx` (`defence_statement`),
  ADD KEY `project_defence_opponent_review_idx` (`opponent_review`),
  ADD KEY `project_defence_supervisor_review_idx` (`supervisor_review`),
  ADD KEY `project_defence_opponent_id_fk` (`opponent_id`);

--
-- Indexy pre tabuľku `project_document`
--
ALTER TABLE `project_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_document_project_id_idx` (`project_id`),
  ADD KEY `project_document_document_id_idx` (`document_id`);

--
-- Indexy pre tabuľku `project_state_history`
--
ALTER TABLE `project_state_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_state_history_project_id_idx` (`project_id`);

--
-- Indexy pre tabuľku `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposals_project_id_idx` (`project_id`),
  ADD KEY `proposals_document_id_idx` (`document_id`);

--
-- Indexy pre tabuľku `proposal_comments`
--
ALTER TABLE `proposal_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_id_idx` (`creator_id`),
  ADD KEY `proposal_id_idx` (`proposal_id`);

--
-- Indexy pre tabuľku `proposal_votes`
--
ALTER TABLE `proposal_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposal_votes_user_id_idx` (`user_id`),
  ADD KEY `proposal_votes_proposal_id_idx` (`proposal_id`);

--
-- Indexy pre tabuľku `role_history`
--
ALTER TABLE `role_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uq` (`email`);

--
-- Indexy pre tabuľku `users_on_project`
--
ALTER TABLE `users_on_project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_on_project_user_id_idx` (`user_id`),
  ADD KEY `users_on_project_project_id_idx` (`project_id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `ad_comments`
--
ALTER TABLE `ad_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `defence_attendance`
--
ALTER TABLE `defence_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `defence_dates`
--
ALTER TABLE `defence_dates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `page_versions`
--
ALTER TABLE `page_versions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `project_defence`
--
ALTER TABLE `project_defence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `project_document`
--
ALTER TABLE `project_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `project_state_history`
--
ALTER TABLE `project_state_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `proposals`
--
ALTER TABLE `proposals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `proposal_votes`
--
ALTER TABLE `proposal_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `role_history`
--
ALTER TABLE `role_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `users_on_project`
--
ALTER TABLE `users_on_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `advertisements`
--
ALTER TABLE `advertisements`
  ADD CONSTRAINT `advertisements_content_fk` FOREIGN KEY (`content`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `advertisements_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `ad_comments`
--
ALTER TABLE `ad_comments`
  ADD CONSTRAINT `ad_comments_ad_id_fk` FOREIGN KEY (`ad_id`) REFERENCES `advertisements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ad_comments_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `defence_attendance`
--
ALTER TABLE `defence_attendance`
  ADD CONSTRAINT `defence_attendance_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_document_id_cs_fk` FOREIGN KEY (`document_id_cs`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `news_document_id_en_fk` FOREIGN KEY (`document_id_en`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `news_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `page_versions`
--
ALTER TABLE `page_versions`
  ADD CONSTRAINT `pages_document_id_cs_fk` FOREIGN KEY (`document_id_cs`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_document_id_en_fk` FOREIGN KEY (`document_id_en`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_page_id_fk` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pages_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_content_fk` FOREIGN KEY (`content`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `project_defence`
--
ALTER TABLE `project_defence`
  ADD CONSTRAINT `project_defence_defence_date_id_fk` FOREIGN KEY (`defence_date_id`) REFERENCES `defence_dates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_defence_statement_fk` FOREIGN KEY (`defence_statement`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_opponent_id_fk` FOREIGN KEY (`opponent_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `project_defence_opponent_review_fk` FOREIGN KEY (`opponent_review`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_statement_author_id_fk` FOREIGN KEY (`statement_author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_defence_supervisor_review_fk` FOREIGN KEY (`supervisor_review`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `project_document`
--
ALTER TABLE `project_document`
  ADD CONSTRAINT `project_document_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `project_document_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `project_state_history`
--
ALTER TABLE `project_state_history`
  ADD CONSTRAINT `project_state_history_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `proposals`
--
ALTER TABLE `proposals`
  ADD CONSTRAINT `proposals_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposals_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `proposal_comments`
--
ALTER TABLE `proposal_comments`
  ADD CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_id` FOREIGN KEY (`proposal_id`) REFERENCES `proposals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `proposal_votes`
--
ALTER TABLE `proposal_votes`
  ADD CONSTRAINT `proposal_votes_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `proposal_votes_proposal_id_fk` FOREIGN KEY (`proposal_id`) REFERENCES `proposals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `role_history`
--
ALTER TABLE `role_history`
  ADD CONSTRAINT `role_history_user_id_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `users_on_project`
--
ALTER TABLE `users_on_project`
  ADD CONSTRAINT `users_on_project_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_on_project_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;