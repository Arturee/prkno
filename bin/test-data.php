<?php
use Nette\Database\Context;
use App\Model\UserRepository;
use App\Model\DocumentRepository;
use App\Model\NewsRepository;
use App\Model\PageVersionRepository;
use App\Model\ProjectRepository;
use App\Model\UserOnProjectRepository;
use App\Model\User;
use Tracy\Debugger;
use Nette\Database;
use Nette\Utils\FileSystem;

/**
 * (cd bin/) php test-data.php
 */


$container = require __DIR__ . '/../app/bootstrap.php';


function echoTime() : void
{
    $seconds = Debugger::timer();
    $min = floor($seconds / 60);
    $sec = $seconds - $min * 60;
    $time = $min . ' min ' . number_format($sec, 2) . ' sec';
    echo $time . "\n";
}

function deleteCacheDir() : void
{
    $cacheDir = __DIR__ . '/../temp/cache';
    FileSystem::delete($cacheDir);
    echo "temp/cache deleted.\n";
}

function quit(Exception $e)
{
    echo "ERROR: " . $e->getMessage();
    exit(1);
}


/**
 * Delete everything in database and then import from prkno.sql
 */
function resetDbSchema($filename)
{
    global $container;
    /** @var Context $database */
    $database = $container->getByType(Context::class);
    try {
        $database->query("
            SET FOREIGN_KEY_CHECKS = 0;
            SET GROUP_CONCAT_MAX_LEN=32768;
            SET @tables = NULL;
            SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables
              FROM information_schema.tables
              WHERE table_schema = (SELECT DATABASE());
            SELECT IFNULL(@tables,'dummy') INTO @tables;
            SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
            PREPARE stmt FROM @tables;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
            SET FOREIGN_KEY_CHECKS = 1;
        ");
        $database = $container->getByType(Context::class);
        $pdo = $database->getConnection()->getPdo();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
        Database\Helpers::loadFromFile($database->getConnection(), $filename);
        echo "Database structure was updated succesfully.\n";
    } catch (Exception $e) {
        quit($e);
    }

}






/*** data **************************************************/













function insertUsers()
{
    global $container;
    $userRepo = $container->getByType(UserRepository::class);
    try {
        $userRepo->insertUser('Petr NovÃ¡k', 'ahoj', 'novakpetr@gmail.com', User::ROLE_STUDENT, null, null);
        $userRepo->insertUser('Petr Å koda', 'ahoj', 'skoda@ksi.mff.cuni.cz', User::ROLE_ACADEMIC, 'Mgr.', null); //doktorand
        $userRepo->insertUser('Martin KruliÅ¡', 'ahoj', 'krulis@ksi.mff.cuni.cz', User::ROLE_COMMITTEE_MEMBER,  'RNDr.', 'Ph.D.');
        $userRepo->insertUser('Petr HnÄ›tynka', 'ahoj', 'hnetynka@d3s.mff.cuni.cz', User::ROLE_COMMITTEE_CHAIR, 'doc. RNDr.', 'Ph.D.');
        echo "Users inserted successfully.\n";
    } catch (Exception $e) {
        quit($e);
    }
}

function insertDocuments()
{
    global $container;
    /** @var DocumentRepository $documentsRepo */
    $documentsRepo = $container->getByType(DocumentRepository::class);
    try {
        $documentsRepo->insertDocument("title 1", "## Document title ##\n- item 1\n- item 2");
        $documentsRepo->insertDocument("title 2", "## Another example. This is markdown ## \n- item xy");
        echo "Documents inserted successfully.\n";

    } catch (Exception $e) {
        quit($e);
    }
}


function insertNews()
{
    global $container;
    /** @var NewsRepository $newsRepo */
    $newsRepo = $container->getByType(NewsRepository::class);
    try {
        $newsRepo->insertNews(
            1,
            "CS titulek 1",
            "- veřejná důležitá novinka novinka",
            "EN title 1",
            "- public important news",
            true,
            true
        );
        $newsRepo->insertNews(
            1,
            "CS nadpis 2",
            "- novinka",
            "EN title 2",
            "## news ##",
            false,
            true
        );
        $newsRepo->insertNews(
            1,
            "CS nadpis 3",
            "- novinka 3",
            "EN title 3",
            "- news",
            true,
            false
        );

        echo "News inserted successfully.\n";
    } catch (Exception $e) {
        quit($e);
    }
}


function insertPageVersions()
{
    global $container;
    $pageVersionRepo = $container->getByType(PageVersionRepository::class);
    try {
        $id = $pageVersionRepo->insertPageVersion(
            "Pravidla projektov",
            "- staticka stranka popisujuca pravidla",
            "Project Rules",
            "- static page describing rules",
            1
        );
        $pageVersionRepo->insertPageVersion(
            "Nove pravidla projektov",
            "- nova verzia popisujuca pravidla",
            "New project Rules",
            "- new static page describing rules",
            1,
            $id
        );
        $pageVersionRepo->insertPageVersion(
            "Prihlasovanie projektov na objahoby",
            "- to ako prihlasovat projekty na obhajoby",
            "Project defence registration",
            "- ho to register project to defence",
            1
        );

        echo "New page inserted successfully.\n";
    } catch (Exception $e) {
        quit($e);
    }
}


/**
 * Also inserts user on project
 */
function insertProject()
{
    global $container;
    $projectRepository = $container->getByType(ProjectRepository::class);
    $userOnProjectRepo = $container->getByType(UserOnProjectRepository::class);
    try {
        $id = $projectRepository->insertProject(
            "web projektove komise", "PRKNO", "popis prkna"
        );
        $userOnProjectRepo->insertUserOnProject($id, 1, "supervisor");

        echo "New project was inserted.\n";
    } catch (Exception $e) {
        quit($e);
    }
}







/*** main ***************************************/


if (!isset($argv[1])){
    /**
     * Compilcated usage to prevent running this while being logged in
     * and having to deal with a crash on the next reload.
     *
     */
    echo "Are you logged out? If not, do it now!\n";
    echo "usage: php test-data.php -l     (-l to confirm that you are logged out)\n";
    exit(1);
}



Debugger::timer();
echo "Refreshing DB (takes ~2 min)\n";

resetDbSchema(__DIR__ . '/prkno.sql');

//data
insertUsers();
insertDocuments();
insertNews();
insertPageVersions();
//insertProject(); //project repo is NOT OK


//deleteCacheDir(); //done in up.sh
echoTime();