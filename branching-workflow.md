Feature branch workflow
	- master is initialized with bootstrap stuff and then never touched
	- each issue/feature is a new branch (eg. f-latte)

	    PULL master
        BRANCH f-latte (locally)
        COMMIT and PUSH often (the branch becomes visible and backed up on server)

		- only one person works on it
		- after a feature is finished, it is reviewed by somebody else and merged.
			- can be done via a "Pull request in GUI", but that's probably an overkill
			- the branch can be deleted locally, but stays alive in bitbucket.
		- Only one person should be doing the merges to avoid conflicts.

            PULL master, f-1
            CHECKOUT master
			MERGE f-1 into it (make sure you DONT --fast-forward. i.e. you should create a new commit even if FF is possible)
			PUSH master

		    - if there are conflicts during merge (the merge created a new commit, but conflicting files stay as
	    	uncommitted changes) - open uncommitted files in editor and resolve conflicts manually. then commit.




    - 5% probability: we can have sub-branches if two people need to work on an issue
    - 5% probability: if master gets updated with good stuff that we need in f-1. we can pull master, checkout f-1,
    merge master into f-1, resolve conflicts.
    - 1% probability: if person A and B merge f1 and f2, it can happen that they merge it locally at the same time,
    then A pushes, then B tries to push, gets rejected. B then needs to pull, and merge remote master to local
    master and then push. This creates a confused branch history (Local/master becomes the new origin/master
    and it has 2 histories - rather confusing).