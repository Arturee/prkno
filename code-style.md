CODE STYLE
- always use type hinting
- indent with 4 spaces
- function parens in C# style
    function hello() : void
    {

    }
- namespaces according to folders
- keywords/highlights:
    TODO - urgent
    MAYBE - less urgent
    PERHAPS - less urgent
    CARE - sth weird is happening, eg. we the code is a workaround for a library bug
    LATER - a todo which cannot/should not be done right now
    TRANSLATE - ze slovenstiny do cestiny

- HTML ids are hyphened (not camel case) eg. id=form-select-date
- null (not NULL)
- commit with linux line endings
- don't use ternary op
- don't return a function call (instead return $result;)
- use @inject only when impossible to use constructor
- always use var type-hints for intellisense

- DON'T CLOSE AN ISSUE IF IT'S NOT FINISHED
- don't merge bugged branch
- don't merge a branch with todos
- always refactor before issue can be closed

- use $form->setTranslator


- if a function is not implemented yet, throw NotImplementedError instead of writing TODOs