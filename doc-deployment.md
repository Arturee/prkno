# AFTER CHECKOUT #
- run ./up.sh



# REQUIREMENTS #
- requires
    - PHP 7.1
    - php_curl extension (for phpCAS)



# REQUIREMENT CHECKER #
- www/checker/index.php checks that requirements for Nette are met
- additional custom requirements in www/checker/custom_checks.php







