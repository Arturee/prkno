see google-drive/PRKNO/Advice

--------------------
# DB #

## Schema and data ##
The DB schema is located in prkno.sql.
To create the DB and fill it with test data run ./bin/test-data.php -l
Each table has the following cols:
```
int id
DateTime created_at
DateTime updated_at
bool deleted
```

## Entities ##
Business logic classes are called entities and they inherit from Entity. To get an entities from the DB, use
a repository class. (Same for insert/delete/update...). Each entity has the following fields:
`$id, $createdAt, $updatedAt`.


## Repositories ##
Only repository classes are allowed to access the database. If you need something from the DB, implement a new
repository class or a new method in an existing class. The current model is one repository class per DB table.
Rows never get deleted, instead col `deleted` is set to TRUE/1 and col `updated_at` is updated to current date-time
and the row is ignored by the application. To revive a row, someone must access the DB directly via phpMyAdmin
and set `deleted` to FALSE/0.


### How to implement one ###
A repository class must
    - be named according to its table (eg. UserRepository)
    - extend Repository
    - contain const TABLE_NAME, which must be the same as the actual SQL table name (eg. `TABLE_NAME = 'Users'`)
    - override `protected function convertRowToEntity(IRow $row)` which converts a DB row into an Entity
    - override `protected function convertEntityToArray($entity): array` which converts an Entity to an array
      of key-value pairs

# Transactions #
There is one transaction per page load. BasePresenter uses app/model/Transaction to start one transaction before
any DB operations take place and commits the transaction after all DB operations are done.
TODO what about AJAXified stuff?
TODO what about abort? - I think a DB exception should cause an automatic abort but not sure









-------------------
# DECLINED STUFF: #
Redirecting to an action based on role. It would be cleaner GUI code, but changes in ACL would need to be
accompanied by changes in code.

We will not use annotation-based access control, because it leads to a messy/hard to understand ACL file,
doesn't work well with components and needs manual checking anyways in some cases (see forum)

SecuredPresenter
    - forces the user to log in
    - if some of it's actions are disallowed for the current user, an exception is thrown
    - We will very probably NOT use this, because we handle everything using components.

For ACL
We dont use neon. simply because we need code completion.



-------------------

# CAS #
- the user gets a token from CAS, which eliminates further redirects while he is still logged in with CAS
- LATER where is the token stored (cookie name/session attr)?
- LATER when does the login expire?



# PRIVILEGE MODEL #
Users access the same presenters and views regardless of their role (eg. Ads:view).
(Instead of a separate view per role (eg. Ads:viewGuest, Ads:viewAdmin, ...))
The view will contain all components, but only the ones that are allowed will be rendered.
This way, if an ACL rule is changed, the GUI changes automatically also. (Tho
there might be a problem with styling, so changing ACL rule may need to be accompanied by
a CSS or latte change). This will be implemented via {if-else} statements in latte (or
inside a presenter if needed).


# PASSING VARS FROM PHP TO JS #
<script type=json>



