# USER DOC #
- auth
    - login/username must be [a-zA-Z0-9]+ and must contain at least one letter

- committee chair
    - user.ban
        - if user writes impolite comments.
    - user.delete
        - (0.1% probability of use)
        - there is no reason for it. (only a CAS bug or a bug in code could be the reason to delete)
        - deleted users will BLOCK others from using their username/email/ukco. If that becomes a problem, an admin
        must delete these users directly from DB or set their values to null.



# ADMIN DOC #
- Error Presenter (list of situations (UserErrors) and how to handle them)
