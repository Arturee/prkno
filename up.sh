#!/usr/bin/env bash

## run:         ./up.sh -L
## force run:   ./up.sh -f
##
##
## If you forgot to log out before running this, go to BasePresenter and uncomment the
## line "$this->getUser()->logout();". Refresh the page and then comment the line out again.
##


if [ "$1" != "-L" ] && [ "$1" != "-f" ]; then
    echo "Make sure you are logged out of PRKNO. If you are, use ./up.sh -L";
    echo "If you have trouble (eg. bower doesn't install all the files it should), use ./up.sh -f";
    exit;
fi


if [ "$1" == "-f" ]; then
    echo ">>> REMOVING /vendor and /www/bower_components";
    find 'vendor' -not -name '.htaccess' -not -name 'web.config' -not -name 'vendor' -delete;
    rm -rf ; rm -rf ./www/bower_components/;
fi


echo ">>> UPDATING CONFIG.LOCAL.NEON"; cp app/config/config.local.neon.copy app/config/config.local.neon;
echo ">>> UPDATING PHP LIBS"; composer update;
echo ">>> UPDATING JS LIBS"; cd www; bower install; cd ..;
echo ">>> UPDATING DB"; php bin/test-data.php -l;
echo ">>> CLEANING CACHE"; rm -r temp/cache;
