<?php

$CODE_DIR = __DIR__ . '/../../';

$tests[] = [
    'title' => 'cas.log',
    'required' => TRUE,
    'passed' => file_exists($CODE_DIR . '/log/cas.log'),
    'message' => 'Present',
    'errorMessage' => 'Absent',
    'description' => 'file log/cas.log must exists - required by CAS library',
];