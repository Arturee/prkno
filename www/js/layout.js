$(function() {
    /**
     * disable disabled forms
     */
    $('.form-disabled').submit(function(){
        return false;
        //prevents submit
    });


    /**
     * hide signin control, but only if it doesn't show any errors
     */
    var SIGNIN_ERROR_SELECTOR = '#frm-signinControl-signinForm .alert';
    var signinErrorIsDisplayed =  $(SIGNIN_ERROR_SELECTOR) && $(SIGNIN_ERROR_SELECTOR).length > 0;
    if(signinErrorIsDisplayed){
        $('#signin').show();
    }

    /**
     * toggle signin control
     */
    $('#link-show-singin').click(function(){
        $('#signin').toggle();
    });
    

    /**
     * Take all divs with markdown class, format markdown content
     * to HTML and replace it
     */
    $('.markdown').each(function(i, e) {
        e.innerHTML = markdown.toHTML(e.innerHTML);
    });
});
